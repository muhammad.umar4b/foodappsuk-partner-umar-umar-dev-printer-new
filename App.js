import React from "react";

import { Provider } from "react-redux";

import Index from "./src/router/Index";
import store from "./src/store/store";

const App = () => {
    return (
        <Provider store={store}>
            <Index />
        </Provider>
    );
};

export default App;
