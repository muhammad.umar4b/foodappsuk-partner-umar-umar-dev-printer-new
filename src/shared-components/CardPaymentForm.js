import React, { useState } from "react";
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";

import globalStyles from "../assets/styles/globalStyles";

import ExpiryDateModal from "./ExpiryDateModal";
import ButtonLoader from "../utilities/ButtonLoader";

export default function CardPaymentForm(props) {
    const [isModalVisible, setModalVisible] = useState(false);

    const {
        cardNumber,
        month,
        year,
        cvc,
        state,
        setState,
        placeOrder,
        monthList,
        yearList,
        isLoading,
    } = props;

    const {
        card,
        boxShadow,
        cardPaymentArea,
        cardHeader,
        cardHeaderLabel,
        paddingTop3,
        paddingBottom1,
        cardPaymentInputLabel,
        cardPaymentInputField,
        continueButtonArea,
        continueButton,
        continueText,
    } = globalStyles;

    return (
        <SafeAreaView>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={[card, boxShadow, cardPaymentArea]}>
                    <View style={cardHeader}>
                        <Text style={cardHeaderLabel}>Payment with card</Text>
                    </View>
                    <View style={paddingTop3}>
                        <Text style={[paddingBottom1, cardPaymentInputLabel]}>
                            Card Number
                        </Text>
                        <TextInput
                            value={cardNumber}
                            placeholder={"Ex. 4242 4242 4242 4242"}
                            style={cardPaymentInputField}
                            keyboardType={"default"}
                            onChangeText={text => setState({ ...state, cardNumber: text })}
                        />
                    </View>
                    <View style={paddingTop3}>
                        <Text style={[paddingBottom1, cardPaymentInputLabel]}>
                            Card Expiry Date
                        </Text>
                        <Pressable onPress={() => setModalVisible(!isModalVisible)}>
                            <TextInput
                                value={`${month}/${year}`}
                                placeholder={"Ex. 01/21"}
                                style={cardPaymentInputField}
                                editable={false}
                                keyboardType={"default"}
                            />
                        </Pressable>
                    </View>
                    <View style={paddingTop3}>
                        <Text style={[paddingBottom1, cardPaymentInputLabel]}>
                            CVC
                        </Text>
                        <TextInput
                            value={cvc}
                            placeholder={"Ex. 312"}
                            style={cardPaymentInputField}
                            keyboardType={"default"}
                            onChangeText={text => setState({ ...state, cvc: text })}
                        />
                    </View>
                    <View style={continueButtonArea}>
                        <TouchableOpacity
                            style={[continueButton]}
                            onPress={() => placeOrder()}
                        >
                            <Text style={continueText}>
                                {isLoading ? <ButtonLoader /> : "Confirm Payment"}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <ExpiryDateModal
                isModalVisible={isModalVisible}
                setModalVisible={setModalVisible}
                monthList={monthList}
                state={state}
                setState={setState}
                month={month}
                year={year}
                yearList={yearList}
            />
        </SafeAreaView>
    );
}
