import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import globalStyles from "../assets/styles/globalStyles";

export default function PaymentMethod({ paymentType, onRadioBtnClick }) {
    const {
        radioButtonArea,
        radioButtonContainer,
        radioButton,
        radioButtonIcon,
        radioButtonContentArea,
        radioButtonImage,
        marginRight1,
        radioButtonText,
        paymentOptionLabel,
    } = globalStyles;

    return (
        <View>
            <Text style={paymentOptionLabel}>Choose a Payment Option</Text>
            <View style={radioButtonArea}>
                {paymentType.map(item => (
                    <View style={radioButtonContainer} key={item.id}>
                        <TouchableOpacity onPress={() => onRadioBtnClick(item)} style={radioButton}>
                            {item.selected ? <View style={radioButtonIcon} /> : null}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => onRadioBtnClick(item)} style={radioButtonContentArea}>
                            {item.img && <Image style={radioButtonImage} source={item.img} />}
                            {item.icon &&
                            <View style={marginRight1}>
                                {item.icon}
                            </View>
                            }
                            <Text style={radioButtonText}>{item.name}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        </View>
    );
}
