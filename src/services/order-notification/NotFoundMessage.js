import React from "react";
import { Text, View } from "react-native";

import styles from "./styles";

const NotFoundMessage = () => {
    const {
        notFoundArea,
    } = styles;

    return (
        <View style={notFoundArea}>
            <Text>Order Information Not found!</Text>
        </View>
    );
};

export default NotFoundMessage;
