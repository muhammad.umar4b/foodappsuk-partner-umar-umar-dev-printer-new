import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "#fff",
            elevation: 3,
            marginHorizontal: wp("5%"),
            marginVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("2%"),
            borderRadius: 10,
        },
        notFoundArea: {
            backgroundColor: "#fff",
            elevation: 3,
            marginHorizontal: wp("5%"),
            marginVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            paddingVertical: hp("2%"),
            borderRadius: 10,
        },
        restaurantName: {
            fontSize: 18,
        },
        acceptButtonArea: {
            marginTop: hp("2%"),
        },
        acceptButton: {
            backgroundColor: "#1fbf21",
            borderRadius: 8,
            height: hp("39%"),
            justifyContent: "center",
            alignItems: "center",
        },
        acceptText: {
            color: "#fff",
            fontSize: 30,
            fontWeight: "700",
        },
        declineButtonArea: {
            marginTop: hp("2%"),
        },
        declineButton: {
            backgroundColor: "#D2181B",
            borderRadius: 8,
            height: hp("39%"),
            justifyContent: "center",
            alignItems: "center",
        },
        declineText: {
            color: "#fff",
            fontSize: 30,
            fontWeight: "700",
        },
    },
);

export default styles;
