import React, {useEffect, useState} from 'react';
import {ScrollView} from 'react-native';

import axios from 'axios';

import styles from '../styles';

import {apiBaseUrl} from '../../../config/index-example';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

import Loader from '../../../utilities/Loader';
import RenderData from './RenderData';

const PendingBookings = ({restaurantId, setTotalPendingBooking}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getAllPendingBookings = async restaurantId => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-booking-pending-orders/${restaurantId}`,
      );
      if (response.data) {
        setData(response.data.data);
        setTotalPendingBooking(response.data.data.length);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    getAllPendingBookings(restaurantId).then(res =>
      console.log('PENDING BOOKINGS: ', res),
    );
  }, [restaurantId]);

  const {bookingsArea} = styles;

  const acceptOrder = async id => {
    try {
      const response = await axios.put(
        `${apiBaseUrl}order/restaurant-accept-order/${id}`,
      );
      if (response.data) {
        showToastWithGravityAndOffset(response.data.message);
        getAllPendingBookings(restaurantId).then(res =>
          console.log('PENDING BOOKINGS: ', res),
        );
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const declineOrder = async id => {
    try {
      const response = await axios.put(
        `${apiBaseUrl}order/restaurant-cancel-order/${id}`,
      );
      if (response.data) {
        showToastWithGravityAndOffset(response.data.message);
        getAllPendingBookings(restaurantId).then(res =>
          console.log('PENDING BOOKINGS: ', res),
        );
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  return (
    <ScrollView style={bookingsArea}>
      {isLoading ? (
        <Loader />
      ) : (
        <RenderData
          data={data}
          acceptOrder={acceptOrder}
          declineOrder={declineOrder}
        />
      )}
    </ScrollView>
  );
};

export default PendingBookings;
