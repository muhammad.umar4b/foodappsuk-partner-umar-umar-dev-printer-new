import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, Text, TouchableOpacity, BackHandler } from "react-native";

import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import { HeaderBackButton } from "@react-navigation/stack";

import styles from "./styles";

import PendingBookings from "./pending-order/PendingBookings";
import AllBookings from "./all-booking/AllBooking";

const MyBookings = ({ navigation, route }) => {
    const [activeTab, setActiveTab] = useState(0);
    const state = useSelector(state => state);
    const [totalAllBooking, setTotalAllBooking] = useState(0);
    const [totalPendingBooking, setTotalPendingBooking] = useState(0);

    useEffect(() => {
        if (route.params && route.params.activeTab) {
            setActiveTab(route.params.activeTab);
        }
    }, [route.params]);

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: (props) => (
                <HeaderBackButton
                    {...props}
                    onPress={() => {
                        if (route.params && Object.keys(route.params).length > 0) {
                            navigation.navigate("Settings");
                        } else {
                            navigation.goBack();
                        }
                    }}
                />
            ),
        });
    }, [navigation]);

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                if (route.params && Object.keys(route.params).length > 0) {
                    navigation.navigate("Settings");
                }
            };
            BackHandler.addEventListener("hardwareBackPress", onBackPress);
            return () =>
                BackHandler.removeEventListener("hardwareBackPress", onBackPress);
        }, []),
    );

    const { restaurant: { restaurantData } } = state;

    const {
        container,
        tabArea,
        tabButton,
        activeTabButton,
        activeTabButtonText,
        tabButtonText,
    } = styles;

    const activeTabStyle = index => activeTab === index ? [tabButton, activeTabButton] : [tabButton];
    const activeTabTextStyle = index => activeTab === index ? [tabButtonText, activeTabButtonText] : [tabButtonText];

    return (
        <View style={container}>
            <View style={tabArea}>
                <TouchableOpacity style={activeTabStyle(0)} onPress={() => setActiveTab(0)}>
                    <Text style={activeTabTextStyle(0)}>
                        All Bookings {totalAllBooking ? `(${totalAllBooking})` : ""}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={activeTabStyle(1)} onPress={() => setActiveTab(1)}>
                    <Text style={activeTabTextStyle(1)}>
                        Pending Bookings {totalPendingBooking ? `(${totalPendingBooking})` : ""}
                    </Text>
                </TouchableOpacity>
            </View>
            {activeTab === 0 && restaurantData &&
                <AllBookings restaurantId={restaurantData._id}
                             setTotalAllBooking={setTotalAllBooking}
                />
            }

            {activeTab === 1 && restaurantData &&
                <PendingBookings restaurantId={restaurantData._id}
                                 setTotalPendingBooking={setTotalPendingBooking}
                />}
        </View>
    );
};

export default MyBookings;
