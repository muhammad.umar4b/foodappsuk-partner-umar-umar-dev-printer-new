import React from "react";
import { Text, View } from "react-native";

import moment from "moment";

import styles from "../styles";

const SingleItem = ({ item, index }) => {
    const {
        bookingsItem,
        bookingsHeader,
        bookingsHeaderText,
        allBookingText,
    } = styles;

    const {
        bookingFor,
        customerName,
        mobileNo,
        bookingDate,
        bookingTime,
        guestNo,
    } = item;

    return (
        <View style={bookingsItem} key={index}>
            <View style={bookingsHeader}>
                <Text style={bookingsHeaderText}>{bookingFor}</Text>
            </View>
            <Text style={allBookingText}>Name: {customerName}</Text>
            <Text style={allBookingText}>Reservation Date: {moment(bookingDate).format(
                "DD/MM/YYYY")}</Text>
            <Text style={allBookingText}>Reservation Time: {bookingTime}</Text>
            <Text style={allBookingText}>Contact No: {mobileNo}</Text>
            <Text style={allBookingText}>Guest No: {guestNo}</Text>
        </View>
    );
};

export default SingleItem;
