import React from "react";
import { Text } from "react-native";

import { Body, Button, Icon, Left, Right, Title, Header } from "native-base";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

const PrinterHeader = ({ navigation, state, setState, savePrinterSettings, printContent }) => {
    const {
        editable,
    } = state;

    const {
        bgWhite,
        textDark,
        textGrey,
    } = globalStyles;

    const {
        editButtonArea,
        editButtonText,
    } = styles;

    return (
        <Header style={bgWhite} androidStatusBarColor="#D2181B">
            <Left>
                <Button onPress={() => navigation.goBack()} transparent>
                    <Icon style={textDark} name="arrow-back" />
                </Button>
            </Left>
            <Body>
                <Title style={textDark}>Printer</Title>
            </Body>
            <Right>
                {editable ?
                    <Button onPress={() => savePrinterSettings()} style={editButtonArea}>
                        <Text style={editButtonText}>Save</Text>
                    </Button>
                    :
                    <Icon onPress={() => setState({ ...state, editable: true })} type="FontAwesome"
                          name="edit" style={textGrey} />
                }
            </Right>
        </Header>
    );
};

export default PrinterHeader;
