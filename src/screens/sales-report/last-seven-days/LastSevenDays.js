import React, {useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';

import axios from 'axios';

import styles from '../styles';
import globalStyles from '../../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../../config/index-example';

// Component
import RenderData from '../RenderData';
import Loader from '../../../utilities/Loader';

const LastSevenDays = ({
  restaurantId,
  setTotalReportLastSevenDays,
  setTotalPrice,
}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getSalesLastSevenDays = async restaurantId => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/get-orders-of-week/${restaurantId}`,
      );
      if (response.data) {
        setData(response.data.data);
        setTotalReportLastSevenDays(response.data.data.length);
        setTotalPrice(response.data.totalSales);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  useEffect(() => {
    getSalesLastSevenDays(restaurantId).then(res =>
      console.log('SALES REPORT LAST SEVEN DAYS: ', res),
    );
  }, [restaurantId]);

  const {reportArea} = styles;

  const {paddingTop2} = globalStyles;

  return (
    <View style={reportArea}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={paddingTop2}>
          {isLoading ? <Loader /> : <RenderData data={data} />}
        </View>
      </ScrollView>
    </View>
  );
};

export default LastSevenDays;
