import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import moment from "moment";

import styles from "../styles";

const SingleItem = (props) => {
    const {
        item,
        index,
        acceptOrder,
        declineOrder,
    } = props;

    const {
        bookingsItem,
        pendingBookingTextArea,
        pendingBookingLabel,
        pendingButtonArea,
        acceptButton,
        acceptButtonText,
        declineButton,
        declineButtonText,
    } = styles;

    const {
        _id,
        customerName,
        bookingFor,
        mobileNo,
        bookingDate,
        bookingTime,
        guestNo,
    } = item;

    return (
        <View style={bookingsItem} key={index}>
            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Name</Text>
                <Text>{customerName}</Text>
            </View>

            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Booking For</Text>
                <Text>{bookingFor}</Text>
            </View>

            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Contact No</Text>
                <Text>{mobileNo}</Text>
            </View>

            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Reservation</Text>
                <Text>{moment(bookingDate).format("DD/MM/YYYY")}</Text>
            </View>

            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Time</Text>
                <Text>{bookingTime}</Text>
            </View>

            <View style={pendingBookingTextArea}>
                <Text style={pendingBookingLabel}>Guest No</Text>
                <Text>{guestNo}</Text>
            </View>

            <View style={pendingButtonArea}>
                <TouchableOpacity style={acceptButton} onPress={() => acceptOrder(_id)}>
                    <Text style={acceptButtonText}>Accept</Text>
                </TouchableOpacity>
                <TouchableOpacity style={declineButton} onPress={() => declineOrder(_id)}>
                    <Text style={declineButtonText}>Decline</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default SingleItem;
