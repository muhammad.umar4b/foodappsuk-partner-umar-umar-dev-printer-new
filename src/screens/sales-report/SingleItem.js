import React from "react";
import { Text, View } from "react-native";

import moment from "moment";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

const SingleItem = ({ item, index }) => {
    const {
        notificationArea,
        notificationTime,
    } = styles;

    const {
        textDark,
    } = globalStyles;

    const {
        orderNumber,
        createdAt,
        totalPrice,
    } = item;

    return (
        <View style={notificationArea} key={index}>
            <View>
                <Text style={textDark}>{orderNumber || "N/A"}</Text>
                <Text style={notificationTime}>
                    {moment(createdAt).format("DD/MM/YYYY hh:mm A") || "N/A"}
                </Text>
            </View>
            <Text style={textDark}>
                £{totalPrice ? parseFloat(totalPrice).toFixed(2) : "0.00"}
            </Text>
        </View>
    );
};

export default SingleItem;
