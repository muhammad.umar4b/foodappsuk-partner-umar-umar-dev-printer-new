import React from "react";
import { Text, View } from "react-native";

import globalStyles from "../../assets/styles/globalStyles";

const TotalPrice = ({ totalPrice }) => {
    const {
        paddingVertical2,
        flexDirectionRow,
        justifyCenter,
        bgWhite,
        marginTop1,
        marginHorizontal5,
    } = globalStyles;

    return (
        <View
            style={[paddingVertical2, flexDirectionRow, justifyCenter, bgWhite, marginTop1, marginHorizontal5]}
        >
            <Text>Total Price : £{totalPrice}</Text>
        </View>
    );
};


export default TotalPrice;
