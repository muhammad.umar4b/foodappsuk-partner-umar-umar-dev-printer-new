import React, {useEffect, useState} from 'react';
import {View, SafeAreaView, ScrollView} from 'react-native';

import axios from 'axios';
import {useSelector} from 'react-redux';

import globalStyles from '../../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../../config/index-example';

import Loader from '../../../utilities/Loader';
import RenderData from './RenderData';

const PendingOrder = ({navigation, setPendingOrder, route}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const state = useSelector(state => state);
  const {
    restaurant: {restaurantData},
  } = state;
  const {_id} = restaurantData;

  const getPendingOrders = async id => {
    try {
      const status = '?status=Pending';
      const response = await axios.get(
        `${apiBaseUrl}order/get-all-orders-by-delivery-status/${id}${status}`,
      );
      if (response.data && !response.data.error) {
        setData(response.data.data);
        setPendingOrder(response.data.data.length);
      }

      setIsLoading(false);
      return true;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getPendingOrders(_id).then(res => console.log('PENDING ORDERS: ', res));
  }, [_id, route]);

  const {flex1, paddingHorizontal5, paddingBottom3} = globalStyles;

  return isLoading ? (
    <Loader />
  ) : (
    <SafeAreaView style={flex1}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[paddingHorizontal5, paddingBottom3]}>
          <RenderData
            navigation={navigation}
            data={data}
            getPendingOrders={getPendingOrders}
            restaurantId={_id}
            restaurantData={restaurantData}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PendingOrder;
