import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  AppState,
  ActivityIndicator,
} from 'react-native';

import PushNotification, {Importance} from 'react-native-push-notification';
import {LocalNotification} from '../../services/LocalNotification';
import axios from 'axios';
import OrderNotification from '../../services/order-notification/OrderNotification';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import globalStyles from '../../assets/styles/globalStyles';
import {useSelector, useDispatch} from 'react-redux';
import {
  getRestaurantCategoryById,
  getRestaurantFoods,
  getRestaurantInfo,
} from '../../store/actions/restaurant';
import {getWeekDay} from '../../utilities/global';
import {SET_ALL_CHARGES, SET_CATEGORIES, SET_FOODS} from '../../store/types';

// Image
import restaurantImage from '../../../assets/resturent.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {playSound} from '../../utilities/functions';
import {BLEPrinter} from 'react-native-thermal-receipt-printer';
import BluetoothStateManager from 'react-native-bluetooth-state-manager';
import {apiBaseUrl} from '../../config/index-example';

const Loader = () => {
  return (
    <View style={styles.loadingArea}>
      <ActivityIndicator size="large" color="#D2181B" />
    </View>
  );
};

const Home = ({navigation}) => {
  const globalState = useSelector(state => state);
  const [isLoading, setIsLoading] = useState(true);
  const [state, setState] = useState({
    name: '',
    logo: '',
    speciality: '',
    address: '',
    openingTime: '',
    closingTime: '',
    isOpen: false,
  });

  const dispatch = useDispatch();
  const {
    auth: {accessToken, activeBluetoothPrinter},
    restaurant: {restaurantData},
  } = globalState;

  const emptyPrinterInfo = () => {
    dispatch({type: 'SET_ACTIVE_PRINTER_INFO', printer: ''});
    dispatch({type: 'SET_PRINTER', printer: ''});
  };

  const printContent = async () => {
    BluetoothStateManager.enable()
      .then(result => {
        BLEPrinter.init()
          .then(() => {
            BLEPrinter.getDeviceList()
              .then(value => {
                console.log(value, 'device list');
                console.log(activeBluetoothPrinter, 'activeBluetoothPrinter');
                if (value && value.length > 0) {
                  const device = value.find(
                    item => item.inner_mac_address === activeBluetoothPrinter,
                  );
                  console.log(device, 'device');
                  if (!device) emptyPrinterInfo();
                } else emptyPrinterInfo();
              })
              .catch(error => {
                emptyPrinterInfo();
                console.log(error, 'get device list error');
              });
          })
          .catch(error => {
            emptyPrinterInfo();
            console.log(error, 'blep printer error');
          });
      })
      .catch(error => {
        emptyPrinterInfo();
        console.log(error, 'state manager error');
      });
    return true;
  };

  useEffect(() => {
    AsyncStorage.getItem('token').then(res => {
      if (res) dispatch({type: 'CHANGE_TOKEN', token: res});
    });

    AsyncStorage.getItem('activePrinterInfo').then(res => {
      if (res)
        dispatch({type: 'SET_ACTIVE_PRINTER_INFO', printer: JSON.parse(res)});
    });

    AsyncStorage.getItem('activeBluetoothPrinter').then(res => {
      if (res) dispatch({type: 'SET_PRINTER', printer: res});
    });

    AsyncStorage.getItem('printerType').then(res => {
      if (res) dispatch({type: 'SET_PRINTER_TYPE', printer: res});
    });

    printContent().then(res => console.log(res, 'res'));
  }, []);

  const {name, logo, address, openingTime, closingTime, postCode, isOpen} =
    state;

  useEffect(() => {
    if (accessToken) {
      axios.interceptors.request.use(
        function (config) {
          config.headers.common['x-auth-token'] = accessToken;
          return config;
        },
        function (error) {
          return Promise.reject(error);
        },
      );

      getRestaurantInfo(dispatch).then(res => {
        console.log('RESTAURANT INFO: ', !!res);

        if (res) {
          setIsLoading(false);
          getInitialData(res['_id'], res['uniId']);

          dispatch({
            type: 'RESTAURANT_INFO_SET',
            restaurantInfo: res._id,
          });

          const {
            name,
            logo,
            address: {address},
            restaurantTiming: {
              restaurantTiming: {available},
            },
            postCode,
          } = res;

          const weekDay = getWeekDay();
          const restaurantTime = available.find(
            item => item['day'] === weekDay,
          );

          setState({
            ...state,
            name,
            logo,
            address,
            openingTime: restaurantTime ? restaurantTime.startTime : 'N/A',
            closingTime: restaurantTime ? restaurantTime.endTime : 'N/A',
            isOpen: restaurantTime ? restaurantTime.isOpen : false,
            postCode: postCode,
          });
        }
      });

      PushNotification.deleteChannel('partner_app_channel');
      PushNotification.createChannel(
        {
          channelId: 'partner_app_channel',
          channelName: 'My channel',
          channelDescription: 'A channel to categorise your notifications',
          playSound: true,
          soundName: 'nuclearalarm.mp3',
          importance: Importance.HIGH,
          vibrate: true,
        },
        created => console.log(`createChannel returned '${created}'`),
      );

      PushNotification.configure({
        onRegister: function (response) {
          console.log('TOKEN:', response);
        },
        onNotification: function (notification) {
          notification.userInteraction = true;
          const {title, message, data} = notification;

          console.log(notification, 'NOTIFICATION');
          console.log(data, 'NOTIFICATION DATA');

          if (notification.userInteraction) {
            if (AppState.currentState !== 'active') {
              navigation.navigate('MyOrder', {pending: true});
            } else if (
              AppState.currentState === 'active' &&
              Object.keys(data).length > 0
            ) {
              playSound();
              navigation.navigate('OrderNotification', {
                data,
                title,
                message,
              });
            }
          } else {
            LocalNotification(notification);
          }

          console.log('onNotification', JSON.stringify(notification));
        },
        onAction: function (notification) {
          console.log('onAction', notification);
        },
        onRegistrationError: function (err) {
          console.error(err.message, err);
        },
        permissions: {
          alert: true,
          badge: true,
          sound: true,
        },
        popInitialNotification: false,
        requestPermissions: true,
      });
    }
  }, [accessToken]);

  const getInitialData = (id, uniId) => {
    getAllCharges(id).then(res => console.log('GET ALL CHARGES : ', res));

    getRestaurantCategoryById(uniId).then(res => {
      console.log('GET ALL CATEGORY : ', !!res);
      const updateCategoryList = res.categories.map(item => {
        item.isActive = false;
        return item;
      });
      dispatch({type: SET_CATEGORIES, payload: updateCategoryList});
    });

    getRestaurantFoods(id).then(res => {
      console.log('GET ALL FOODS : ', !!res);
      dispatch({type: SET_FOODS, payload: res.foods});
    });
  };

  const getAllCharges = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}charge/get-all-charges/${id}`,
      );
      if (response.data) {
        if (response.data.data.length) {
          dispatch({type: SET_ALL_CHARGES, payload: response.data.data[0]});
        }
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const {
    container,
    topBar,
    topBar1,
    restaurantLogo,
    restaurantNameText,
    restaurantTime,
    restaurantButtonArea,
    houseOrderButton,
    houseOrderButtonText,
  } = styles;

  const {f16, fw700} = globalStyles;

  const renderItem = (
    <View style={container}>
      <Image
        source={logo ? {uri: logo} : restaurantImage}
        style={restaurantLogo}
      />
      <Text style={restaurantNameText}>{name || 'N/A'}</Text>
      <Text style={[f16, fw700]}>
        {address || 'N/A'}, {postCode || 'N/A'}
      </Text>
      {isOpen ? (
        <View style={restaurantTime}>
          <Text>
            {openingTime || 'N/A'}-{closingTime || 'N/A'}
          </Text>
        </View>
      ) : (
        <View style={restaurantTime}>
          <Text>Restaurant is off today!</Text>
        </View>
      )}
      <View style={restaurantButtonArea}>
        <TouchableOpacity
          style={houseOrderButton}
          onPress={() => navigation.navigate('RestaurantDetails')}>
          <Text style={houseOrderButtonText}>In-House Order</Text>
        </TouchableOpacity>
      </View>
      {/* {!printerStatus ? (
        <View style={restaurantButtonArea}>
          <View style={{marginTop: '10%'}}>
            <Text style={{color: 'black', fontSize: 17}}>
              Printer Status Disable
            </Text>
          </View>
        </View>
      ) : (
        <View style={restaurantButtonArea}>
          <View style={{marginTop: '10%'}}>
            <Text style={{color: 'black', fontSize: 17}}>
              Printer Status Enable
            </Text>
          </View>
        </View>
      )} */}
    </View>
  );

  return (
    <>
      <View style={topBar} />
      {isLoading ? <Loader /> : renderItem}
      <View style={topBar1}>
        <View style={restaurantButtonArea}>
          <View style={{marginTop: '2%'}}>
            <Text style={{color: 'white', fontSize: 17}}>
              Printer Status {activeBluetoothPrinter ? 'Enable' : 'Disable'}
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topBar: {
    height: hp('1%'),
    width: wp('100%'),
    backgroundColor: '#d2181b',
  },
  topBar1: {
    height: hp('5%'),
    width: wp('100%'),
    backgroundColor: '#d2181b',
  },
  restaurantLogo: {
    height: 150,
    width: 150,
    borderRadius: 75,
    backgroundColor: '#000',
  },
  restaurantNameText: {
    fontWeight: 'bold',
    fontSize: 24,
    paddingTop: hp('3%'),
    color: '#D2181B',
  },
  restaurantTime: {
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('1%'),
    marginBottom: hp('2%'),
  },
  restaurantButtonArea: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  houseOrderButton: {
    backgroundColor: '#d2181b',
    paddingHorizontal: wp('5%'),
    paddingVertical: hp('1.5%'),
    marginRight: wp('2%'),
    borderRadius: 20,
  },
  houseOrderButtonText: {
    color: '#fff',
  },
  loadingArea: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Home;
