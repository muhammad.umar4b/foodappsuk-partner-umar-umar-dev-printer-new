import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, StyleSheet, BackHandler } from "react-native";
import AllOrders from "./AllOrders";
import PendinOrders from "./PendingOrders";
import { Tabs, Tab } from "native-base";
import { useFocusEffect } from "@react-navigation/native";
import { HeaderBackButton } from "@react-navigation/stack";
import DineInOrder from "../my-order/DineInOrder";

const Orders = ({ navigation, route }) => {
    const [activeTab, setActiveTab] = useState(null);

    useEffect(() => {
        if (route.params && route.params.activeTab) {
            setActiveTab(route.params.activeTab);
        } else setActiveTab(0);
    }, []);

    useLayoutEffect(() => {
        navigation.setOptions({
            headerLeft: (props) => (
                <HeaderBackButton
                    {...props}
                    onPress={() => {
                        if (route.params && Object.keys(route.params).length > 0) {
                            navigation.navigate("Settings");
                        } else {
                            navigation.goBack();
                        }
                    }}
                />
            ),
        });
    }, [navigation]);

    useFocusEffect(
        React.useCallback(() => {
            const onBackPress = () => {
                if (route.params && Object.keys(route.params).length > 0) {
                    navigation.navigate("Settings");
                }
            };
            BackHandler.addEventListener("hardwareBackPress", onBackPress);
            return () =>
                BackHandler.removeEventListener("hardwareBackPress", onBackPress);
        }, []),
    );

    return (
        activeTab !== null && <View style={styles.container}>
            <Tabs tabBarUnderlineStyle={{ backgroundColor: "#d2181b" }} initialPage={activeTab}>
                <Tab textStyle={{ color: "black" }} activeTextStyle={{ color: "#d2181b" }} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }}
                     heading="All Orders">
                    <AllOrders navigation={navigation} />
                </Tab>
                <Tab textStyle={{ color: "black" }} activeTextStyle={{ color: "#d2181b" }} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }}
                     heading="Pending Orders">
                    <PendinOrders navigation={navigation} />
                </Tab>
                <Tab textStyle={{ color: "black" }} activeTextStyle={{ color: "#d2181b" }} tabStyle={{ backgroundColor: "white" }} activeTabStyle={{ backgroundColor: "white" }}
                     heading="Dine-In">
                    <DineInOrder navigation={navigation} route={route} />
                </Tab>
            </Tabs>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
    },
});

export default Orders;
