import React, { Component } from 'react'
import { View, Text,StyleSheet,TextInput,TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import {Card,CardItem,Body,H3,Radio} from "native-base"

export class AddNewDelivery extends Component {
    state = {
        name:'',
        address:'',
        postCode:'',
        phone:'',
        itemPrice:'',
        card:true,
        cash:false
    }
    render() {
        return (
            <View style={styles.container}>
                <Card>
                    <CardItem>
                        <Body>
                            <H3 style={{fontWeight:'bold'}}>Delivery Address</H3>
                            <TextInput placeholder={'Name'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',paddingLeft:10,width:'100%',padding:5,margin:5}} />
                            <TextInput placeholder={'Address'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',width:'100%',padding:5,paddingLeft:10,margin:5}} />
                            <TextInput placeholder={'Post Code'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',width:'100%',padding:5,paddingLeft:10,margin:5}} />
                            <TextInput placeholder={'Guest Number'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',width:'100%',padding:5,paddingLeft:10,margin:5}} />
                            <TextInput placeholder={'Item Price £'} style={{borderBottomWidth:1,borderBottomColor:'#adadad',width:'100%',padding:5,paddingLeft:10,margin:5}} />
                            <View style={{width:'100%',flexDirection:'row',justifyContent:'space-around',marginTop:20,marginBottom:20}}>
                            <Radio
                color={"#d2181b"}
                selectedColor={"#d2181b"}
                selected={this.state.card}
                onPress={()=>{
                    if(this.state.card){
                        this.setState({card:false,cash:true});
                    }else{
                        this.setState({card:true,cash:false});
                    }

                }}
              />
              <Text>Cash</Text>
              <Radio
                color={"#d2181b"}
                selectedColor={"#d2181b"}
                selected={this.state.cash}
                onPress={()=>{
                    if(this.state.card){
                        this.setState({card:false,cash:true});
                    }else{
                        this.setState({card:true,cash:false});
                    }
                }}
              />
              <Text>Card</Text>
                            </View>
                            <TouchableOpacity style={{padding:10,borderRadius:5,alignSelf:'flex-end',backgroundColor:'#d2181b'}}><Text style={{color:'#fff'}}>Place Delivery</Text></TouchableOpacity>
                        </Body>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#ffffff'
    }
})

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewDelivery)
