import React, { Component } from 'react'
import { View, Text,StyleSheet,StatusBar,TextInput,Dimensions } from 'react-native'
import { connect } from 'react-redux'
import {Icon} from "native-base"

const {height,width} = Dimensions.get('window');

export class Chat extends Component {
    state = {
        message:''
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor="black" />
                <View style={{height:10,position:'absolute',top:0,left:0,width:'100%',backgroundColor:'#d2181b'}}></View>
                <View style={{height:60,position:'absolute',top:10,elevation:10,left:0,width:'100%',backgroundColor:'#ffffff'}}>
                    <Text style={{fontSize:20,fontWeight:'bold',marginTop:10,marginLeft:10}}>Live Support</Text>
                </View>
                <View style={{position:'absolute',top:height-120,left:0,width:'100%',zIndex:10}}>
               
                    <TextInput
        style={{padding:10,borderTopWidth:2,borderBottomWidth:2,borderColor:'#e4e4e4'}}
        onChangeText={value=>{this.setState({message:value})}}
        value={this.state.message}
        placeholder="Type your message here"
        
      />
                </View>
                <Icon type="Ionicons" name="send" style={{fontSize:24,color:'424242',position:'absolute',top:height-110,right:30,zIndex:12}} />
                
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        justifyContent:'center'
    }
})

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)
