import React from "react";
import { ScrollView, Text, TouchableOpacity, View, TextInput, Pressable } from "react-native";

import Modal from "react-native-modal";

import styles from "../styles";
import globalStyles from "../../../assets/styles/globalStyles";

const timings = [
    "20",
    "30",
    "40",
];

const CollectionTimeModal = (props) => {
    const {
        collectionTimeModalVisible,
        setCollectionTimeModalVisible,
        state,
        setState,
        inputEditable,
    } = props;

    const {
        pickupTime,
    } = state;

    const {
        modalView,
        modalBody,
        modalFooter,
        monthArea,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        monthAreaHeaderText,
        closeButton,
        continueText,
        inputLabel,
        inputField
    } = styles;

    const {
        paddingBottom1,
        paddingTop3
    } = globalStyles;

    return (
        <>
            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>Collection Time</Text>
                <Pressable onPress={() => inputEditable && setCollectionTimeModalVisible(!collectionTimeModalVisible)}>
                    <TextInput
                        value={pickupTime}
                        placeholder={"Collection Time"}
                        editable={false}
                        style={inputField}
                        keyboardType={"default"}
                    />
                </Pressable>
            </View>
            <Modal
                isVisible={collectionTimeModalVisible}
                style={modalView}>
                <View>
                    <View style={modalBody}>
                        <View style={monthArea}>
                            <Text style={monthAreaHeaderText}>Collection Time</Text>
                            <ScrollView style={monthAreaContent}>
                                {timings.map((item, index) => (
                                    <TouchableOpacity
                                        style={monthAreaContentBlock}
                                        key={index}
                                        onPress={() => setState({ ...state, pickupTime: item })}
                                    >
                                        <Text
                                            style={[monthAreaContentText, pickupTime === item && monthAreaContentTextActive]}>
                                            {item}
                                        </Text>
                                    </TouchableOpacity>
                                ))}
                            </ScrollView>
                        </View>
                    </View>
                    <View style={modalFooter}>
                        <TouchableOpacity
                            style={closeButton}
                            onPress={() => setCollectionTimeModalVisible(!collectionTimeModalVisible)}
                        >
                            <Text style={continueText}>Ok</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </>
    );
};

export default CollectionTimeModal;
