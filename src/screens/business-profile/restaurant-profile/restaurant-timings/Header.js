import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

const Header = (props) => {
    const {
        inputEditable,
        updateRestaurantTimings,
    } = props;

    const {
        cardHeaderLabel,
        cardHeaderEditText,
    } = styles;

    const {
        cardHeader,
        marginRight1,
        paddingTop05,
        flexDirectionRow,
        paddingTop0,
    } = globalStyles;

    return (
        <View style={cardHeader}>
            <Text style={[cardHeaderLabel, paddingTop0]}>
                Available Days
            </Text>

            <TouchableOpacity style={[flexDirectionRow, paddingTop05]} onPress={() => updateRestaurantTimings()}>
                {!inputEditable &&
                    <MaterialIcons name="edit" size={20} color="#555555" />
                }

                {inputEditable &&
                    <Feather style={marginRight1} name="check-circle" size={20} color="#555555" />
                }

                <Text style={cardHeaderEditText}>
                    {inputEditable ? "Save" : "Edit"}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

export default Header;
