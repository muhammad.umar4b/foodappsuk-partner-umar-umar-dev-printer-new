import React from "react";
import { Switch, Text, View } from "react-native";

import styles from "./styles";

const SwitchButton = (props) => {
    const {
        label,
        value,
        name,
        state,
        setState,
        disabled,
    } = props;

    const {
        switchButtonArea,
        switchButtonAreaLabel,
    } = styles;

    return (
        <View style={switchButtonArea}>
            <Text style={switchButtonAreaLabel}>
                {label}
            </Text>

            <Switch
                trackColor={{ false: "#767577", true: "#D2181B" }}
                thumbColor={value ? "#ffffff" : "#f4f3f4"}
                ios_backgroundColor="#3e3e3e"
                onValueChange={() => setState({ ...state, [name]: !value })}
                value={value}
                disabled={disabled}
            />
        </View>
    );
};

export default SwitchButton;
