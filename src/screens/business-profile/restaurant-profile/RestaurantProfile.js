import React, { useEffect, useState } from "react";
import { ScrollView, SafeAreaView } from "react-native";

import { useDispatch } from "react-redux";

import globalStyles from "../../../assets/styles/globalStyles";

import { showToastWithGravityAndOffset } from "../../../shared-components/ToastMessage";
import { getRestaurantInfo, updateRestaurantAddress } from "../../../store/actions/restaurant";

// Component
import Loader from "../../../utilities/Loader";
import AddressInfo from "./address-info/AddressInfo";
import RestaurantTimings from "./restaurant-timings/RestaurantTimings";

const RestaurantProfile = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [addressInputEditable, setAddressInputEditable] = useState(false);
    const [timingsInputEditable, setTimingsInputEditable] = useState(false);

    const [state, setState] = useState({
        id: "",
        name: "",
        logo: "",
        address: "",
        postCode: "",
        phoneNo: "",
        isSundayAvailable: false,
        isMondayAvailable: false,
        isTuesdayAvailable: false,
        isWednesdayAvailable: false,
        isThursdayAvailable: false,
        isFridayAvailable: false,
        isSaturdayAvailable: false,
        deliveryTimes: "",
        collectionTime: "",
        bookingStartTime: "",
        bookingEndTime: "",
        isDeliveryAvailable: false,
        isRiderService: false,
        areaLimit: "",
        deliveryCharge: "",
    });

    const dispatch = useDispatch();

    const getRestaurant = () => {
        getRestaurantInfo(dispatch)
            .then(res => {
                console.log("RESTAURANT INFO: ", !!res);

                const {
                    _id,
                    name,
                    logo,
                    address: {
                        address,
                        phoneNo,
                        postCode,
                    },
                    restaurantTiming: {
                        isSundayAvailable,
                        isMondayAvailable,
                        isTuesdayAvailable,
                        isWednesdayAvailable,
                        isThursdayAvailable,
                        isFridayAvailable,
                        isSaturdayAvailable,
                        deliveryTimes,
                        collectionTime,
                        bookingStartTime,
                        bookingEndTime,
                    },
                    restaurantDelivery: {
                        isDeliveryAvailable,
                        isRiderService,
                        areaLimit,
                        deliveryCharge,
                    },
                } = res;

                const updateState = {
                    _id,
                    name,
                    logo,
                    addressId: res.address._id,
                    address,
                    phoneNo,
                    postCode,
                    isSundayAvailable,
                    isMondayAvailable,
                    isTuesdayAvailable,
                    isWednesdayAvailable,
                    isThursdayAvailable,
                    isFridayAvailable,
                    isSaturdayAvailable,
                    deliveryTimes,
                    collectionTime,
                    bookingStartTime,
                    bookingEndTime,
                    isDeliveryAvailable,
                    isRiderService,
                    areaLimit,
                    deliveryCharge,
                };

                setState(updateState);
                setIsLoading(false);
            });
    };

    useEffect(() => {
        getRestaurant();
    }, []);

    const updateRestaurantData = () => {
        getRestaurantInfo(dispatch).then(res => console.log("RESTAURANT INFO: ", !!res));
    };

    const updateRestaurantAddressInfo = (payload) => {
        const {
            addressId,
        } = state;

        updateRestaurantAddress(addressId, payload, dispatch)
            .then(res => {
                console.log("RESTAURANT UPDATE: ", res);

                if (res) {
                    setAddressInputEditable(!addressInputEditable);
                    showToastWithGravityAndOffset("Address Updated Successfully!");
                    updateRestaurantData();
                }
            });
    };

    const updateRestaurantTimingsInfo = (payload) => {

    };

    const {
        marginTop5,
    } = globalStyles;

    return isLoading ? <Loader style={marginTop5} /> :
        <SafeAreaView>
            <ScrollView showsVerticalScrollIndicator={false}>
                <AddressInfo
                    state={state}
                    setState={setState}
                    addressInputEditable={addressInputEditable}
                    setAddressInputEditable={setAddressInputEditable}
                    updateRestaurantAddressInfo={updateRestaurantAddressInfo}
                />

                {/*<RestaurantTimings
                    state={state}
                    setState={setState}
                    timingsInputEditable={timingsInputEditable}
                    setTimingsInputEditable={setTimingsInputEditable}
                    updateRestaurantTimingsInfo={updateRestaurantTimingsInfo}
                />*/}
            </ScrollView>
        </SafeAreaView>;
};

export default RestaurantProfile;
