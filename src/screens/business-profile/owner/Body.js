import React from "react";
import { Text, TextInput, View } from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

const Body = (props) => {
    const {
        state,
        setState,
        inputEditable,
    } = props;

    const {
        inputLabel,
        inputField,
    } = styles;

    const {
        paddingTop3,
        paddingBottom1,
    } = globalStyles;

    const {
        email,
        mobileNo,
        ownerName,
    } = state;

    return (
        <>
            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Email
                </Text>
                <TextInput
                    value={email}
                    placeholder={"E-mail"}
                    editable={false}
                    style={inputField}
                    keyboardType={"default"}
                />
            </View>

            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Full Name
                </Text>
                <TextInput
                    value={ownerName}
                    placeholder={"Name"}
                    editable={inputEditable}
                    style={inputField}
                    keyboardType={"default"}
                    onChangeText={text => setState({ ...state, ownerName: text })}
                />
            </View>

            <View style={paddingTop3}>
                <Text style={[paddingBottom1, inputLabel]}>
                    Contact Number
                </Text>
                <TextInput
                    value={mobileNo}
                    placeholder={"Mobile"}
                    editable={inputEditable}
                    style={inputField}
                    keyboardType={"default"}
                    onChangeText={text => setState({ ...state, mobileNo: text })}
                />
            </View>
        </>
    );
};

export default Body;
