import React, { Component } from "react";
import {
    ActivityIndicator,
    Image,
    Modal,
    Pressable,
    SafeAreaView,
    ScrollView,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View,
} from "react-native";
import { connect } from "react-redux";
import { CheckBox, Input } from "react-native-elements";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Icon } from "native-base";
import PushNotification from "react-native-push-notification";

export class Login extends Component {
    state = {
        showPassword: true,
        requestLoading: false,
        agree: false,
        email: "",
        password: "",
        type: "owner",
        modalVisible: false,
        deviceToken: "",
    };

    componentDidMount() {
        let that = this;
        PushNotification.configure({
            onRegister: function(response) {
                console.log("TOKEN : ", response);
                that.setState({deviceToken: response.token});
            },
            onRegistrationError: function(err) {
                console.error(err.message, err);
            },
        });
    }

    submitLogin = () => {
        this.setState({ requestLoading: true });
        const host = this.props.host;
        console.log("host", host);
        return fetch(host, {
            // return fetch(host + "login", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password,
                type: this.state.type,
                deviceToken: this.state.deviceToken,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                // console.log("submitLogin")

                if (responseJson.hasOwnProperty("error")) {
                    this.setState({ requestLoading: false });
                    ToastAndroid.show(responseJson.error.toString(), ToastAndroid.SHORT, ToastAndroid.TOP);
                } else {
                    console.log("responseJson.user", responseJson);
                    console.log(responseJson["data"], 52);
                    this.setState({ requestLoading: false });
                    AsyncStorage.multiSet([["token", responseJson.token], ["email", responseJson["data"].email], ["loggedIn", "true"], ["id", responseJson["data"]._id.toString()]]).then(() => {
                        // AsyncStorage.multiSet([["token", responseJson.token], ["email", responseJson.user.email], ["loggedIn", "true"], ["id", responseJson.user._id.toString()]]).then(() => {
                        this.props.changeInfo({
                            id: responseJson["data"]._id,
                            email: responseJson["data"].email,
                            // id: responseJson.user._id,
                            // email: responseJson.user.email,
                        });
                        console.log("responseJson['data'].restaurant", responseJson["data"].restaurant);
                        this.props.changeAccessRESTAURANT_INFO_SET1(responseJson["data"].restaurant);
                        this.props.changeAccessToken(responseJson.token);
                        this.props.changeLogged(true);
                        this.props.setUser(responseJson["data"].ownerName);
                        // this.props.setUser(responseJson.user);
                    });

                }
            })
            .catch((error) => {
                console.log(error);
            });
    };

    render() {
        return (
            <SafeAreaView>
                <ScrollView showsHorizontalScrollIndicator={false}
                            style={{ backgroundColor: "white", paddingBottom: 100 }}>
                    <View style={styles.container}>
                        <Image source={require("../../../assets/iconstack.png")} style={{ height: 250, width: 250 }} />
                        <Input
                            placeholder="Email"
                            onChangeText={value => {
                                this.setState({ email: value });
                            }}
                            inputContainerStyle={styles.sizingInput}
                            leftIcon={
                                <Icon type="MaterialIcons" name="email" style={{ fontSize: 24, color: "black" }} />
                            }
                        />
                        <Input
                            placeholder="Password"
                            onChangeText={value => {
                                this.setState({ password: value });
                            }}
                            inputContainerStyle={styles.sizingInput}
                            leftIcon={
                                <Icon type="AntDesign" name="lock" style={{ fontSize: 24, color: "black" }} />
                            }
                            secureTextEntry={this.state.showPassword}
                            rightIcon={
                                !this.state.showPassword ? <Icon type="Entypo" name="eye" onPress={() => {
                                        this.setState({ showPassword: true });
                                    }} style={{ fontSize: 24, color: "black" }} /> :
                                    <Icon type="Entypo" name="eye-with-line" onPress={() => {
                                        this.setState({ showPassword: false });
                                    }} style={{ fontSize: 24, color: "black" }} />

                            }
                        />
                        <View style={{
                            alignSelf: "flex-start",
                            marginLeft: 25,
                            flexDirection: "row",
                            justifyContent: "space-around",
                        }}>
                            <CheckBox
                                containerStyle={{ backgroundColor: "white", borderColor: "white" }}
                                title="I agree with"
                                checkedColor={"#d2181b"}
                                onPress={() => this.setState({ agree: !this.state.agree })}
                                checked={this.state.agree}
                            />
                            <TouchableOpacity onPress={() => {
                                this.setState({ modalVisible: true });
                            }}><Text style={{ color: "#d2181b", fontWeight: "bold", marginTop: 20, marginLeft: -25 }}>Covid-19
                                Guidelines.</Text></TouchableOpacity>
                        </View>

                        {/*<TouchableOpacity onPress={() => {
                            this.props.navigation.navigate("ForgotPassword");
                        }} style={{ alignSelf: "flex-end", marginRight: 40, marginBottom: 10 }}><Text
                            style={{ fontSize: 14, fontWeight: "bold" }}>Forget Password</Text></TouchableOpacity>*/}
                        <TouchableOpacity onPress={() => {
                            this.submitLogin();
                        }} style={styles.pressable}>
                            {this.state.requestLoading ? <ActivityIndicator size="small" color="white" /> :
                                <Text style={{ color: "white", fontWeight: "bold" }}>SIGN IN</Text>}

                        </TouchableOpacity>

                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {

                            this.setState({ modalVisible: false });
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>Covid-19 Guidelines</Text>
                                <Text style={{ fontSize: 18, color: "#000000", lineHeight: 24 }}>
                                    1. Do you wash & sanitise your hands before & after every order ?{"\n"}{"\n"}

                                    2. Do you clean & disinfect your kitchen regularly? {"\n"}{"\n"}

                                    3. Do you wear a face mask & head cover when working? {"\n"}{"\n"}

                                    4. Do you maintain 2 meters social distance at your workplace?
                                </Text>
                                <Pressable
                                    style={{
                                        padding: 15,
                                        paddingLeft: 25,
                                        paddingRight: 25,
                                        elevation: 10,
                                        color: "white",
                                        backgroundColor: "#d2181b",
                                        borderRadius: 30,
                                        position: "absolute",
                                        bottom: 20,
                                        right: 30,
                                    }}
                                    onPress={() => this.setState({ modalVisible: false, agree: true })}
                                >
                                    <Text style={styles.textStyle}>Agree</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        host: state.auth.host,
    };
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff",
        paddingBottom: 50,
    },
    sizingInput: {
        borderColor: "white",
        backgroundColor: "white",
        elevation: 10,
        padding: 2,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 10,
        marginLeft: 30,
        marginRight: 30,
        borderWidth: 1,
    },
    pressable: {
        width: "80%",
        elevation: 10,
        padding: 15,
        borderRadius: 10,
        alignItems: "center",
        backgroundColor: "#d2181b",
        marginLeft: 30,
        marginRight: 30,

    },
    centeredView: {
        flex: 1,
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        height: 500,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
    },
    modalText: {
        marginBottom: 15,
        alignSelf: "flex-start",
        fontSize: 22,
        fontWeight: "bold",
        textAlign: "left",
    },
});

const mapDispatchToProps = dispatch => {
    return {
        changeAccessToken: (value) => {
            dispatch({ type: "CHANGE_TOKEN", token: value });
        },
        changeAccessRESTAURANT_INFO_SET1: (value) => {
            dispatch({ type: "RESTAURANT_INFO_SET1", restaurantInfo1: value });
        },
        changeLogged: (value) => {
            dispatch({ type: "LOGIN", logged: value });
        },
        changeInfo: (value) => {
            dispatch({ type: "PROFILE_SET", info: value });
        },
        setUser: (value) => {
            dispatch({ type: "SET_USER", payload: value });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
