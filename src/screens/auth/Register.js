import React, { Component } from "react";
import { View, Text, StyleSheet, StatusBar, Image, TouchableOpacity, SafeAreaView, ScrollView, Modal, Pressable } from "react-native";
import { connect } from "react-redux";
import { Input, CheckBox } from "react-native-elements";
import { Icon } from "native-base";

export class Register extends Component {
    state = { showPassword: false, agree: false, modalVisible: false };

    render() {
        return (
            <SafeAreaView>
                <ScrollView showsHorizontalScrollIndicator={false} style={{ backgroundColor: "white", paddingBottom: 100 }}>
                    <View style={styles.container}>
                        <Image source={require("../../../assets/iconstack.png")} style={{ height: 250, width: 250 }} />
                        <Input
                            placeholder="Email"
                            inputContainerStyle={styles.sizingInput}
                            leftIcon={
                                <Icon type="MaterialIcons" name="email" style={{ fontSize: 24, color: "black" }} />
                            }
                        />
                        <Input
                            placeholder="Password"
                            inputContainerStyle={styles.sizingInput}
                            leftIcon={
                                <Icon type="AntDesign" name="lock" style={{ fontSize: 24, color: "black" }} />
                            }
                            secureTextEntry={true}
                            rightIcon={
                                !this.state.showPassword ? <Icon type="Entypo" name="eye-with-line" onPress={() => {
                                    this.setState({ showPassword: true });
                                }} style={{ fontSize: 24, color: "black" }} /> : <Icon type="Feather" name="eye" onPress={() => {
                                    this.setState({ showPassword: false });
                                }} style={{ fontSize: 24, color: "black" }} />

                            }
                        />
                        <Input
                            placeholder="Confirm Password"
                            inputContainerStyle={styles.sizingInput}
                            leftIcon={
                                <Icon type="AntDesign" name="lock" style={{ fontSize: 24, color: "black" }} />
                            }
                            secureTextEntry={true}
                            rightIcon={
                                !this.state.showPassword ? <Icon type="Entypo" name="eye-with-line" onPress={() => {
                                    this.setState({ showPassword: true });
                                }} style={{ fontSize: 24, color: "black" }} /> : <Icon type="Feather" name="eye" onPress={() => {
                                    this.setState({ showPassword: false });
                                }} style={{ fontSize: 24, color: "black" }} />

                            }
                        />
                        <TouchableOpacity style={styles.pressable}>
                            <Text style={{ color: "white", fontWeight: "bold" }}>SIGN UP</Text>
                        </TouchableOpacity>
                        <Text style={{ marginTop: 15, fontWeight: "bold" }}>Or join with</Text>
                        <View style={{ flexDirection: "row", marginTop: 20 }}>
                            <TouchableOpacity style={{ marginRight: 10, elevation: 10, backgroundColor: "white", borderRadius: 25 }}>
                                <Image source={require("../../../assets/img/facebook.png")} style={{ height: 50, width: 50 }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ backgroundColor: "white", padding: 10, borderRadius: 30, elevation: 5, alignItems: "center", justifyContent: "center" }}>
                                <Image source={require("../../../assets/img/gmail.png")} style={{ height: 30, width: 30 }} />
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: "row", marginTop: 80 }}>
                            <Text style={{ fontSize: 20, marginRight: 5 }}>Already have an account?</Text>
                            <TouchableOpacity onPress={() => {
                                this.props.navigation.navigate("Login");
                            }}><Text style={{ fontSize: 20, textDecorationLine: "underline" }}>Sign In</Text></TouchableOpacity>

                        </View>
                    </View>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {

                            this.setState({ modalVisible: false });
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>Covid-19 Guidelines</Text>
                                <Text style={{ fontSize: 18, color: "#000000", lineHeight: 24 }}>
                                    1. Do you wash & sanitise your hands before & after every order ?{"\n"}{"\n"}

                                    2. Do you clean & disinfect your kitchen regularly? {"\n"}{"\n"}

                                    3. Do you wear a face mask & head cover when working? {"\n"}{"\n"}

                                    4. Do you maintain 2 meters social distance at your workplace?
                                </Text>
                                <Pressable
                                    style={{
                                        padding: 15,
                                        paddingLeft: 25,
                                        paddingRight: 25,
                                        elevation: 10,
                                        color: "white",
                                        backgroundColor: "#d2181b",
                                        borderRadius: 30,
                                        position: "absolute",
                                        bottom: 20,
                                        right: 30,
                                    }}
                                    onPress={() => this.setState({ modalVisible: false, agree: true })}
                                >
                                    <Text style={styles.textStyle}>Agree</Text>
                                </Pressable>
                            </View>
                        </View>
                    </Modal>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state) => ({});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#ffffff",
        paddingBottom: 50,
    },
    sizingInput: {
        borderColor: "white",
        backgroundColor: "white",
        elevation: 10,
        padding: 2,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 10,
        marginLeft: 30,
        marginRight: 30,
        borderWidth: 1,
    },
    pressable: {
        width: "80%",
        elevation: 10,
        padding: 15,
        borderRadius: 10,
        alignItems: "center",
        backgroundColor: "#d2181b",
        marginLeft: 30,
        marginRight: 30,

    },
    centeredView: {
        flex: 1,
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        height: 500,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
    },
    modalText: {
        marginBottom: 15,
        alignSelf: "flex-start",
        fontSize: 22,
        fontWeight: "bold",
        textAlign: "left",
    },
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
