import React, {useEffect, useLayoutEffect, useState} from 'react';
import {View, Text, SafeAreaView, ScrollView} from 'react-native';

import styles from './styles';
import globalStyles from '../../assets/styles/globalStyles';

import _ from 'lodash';
import axios from 'axios';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {useSelector} from 'react-redux';
import {Badge} from 'react-native-elements';

import {apiBaseUrl} from '../../config/index-example';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

// Component
import Loader from '../../utilities/Loader';
import RenderData from './RenderData';

const Notifications = ({navigation}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const state = useSelector(state => state);
  const {
    auth: {userId},
  } = state;

  const {bgRed, marginRight4, marginTop2, paddingLeft5} = globalStyles;

  const {container} = styles;

  const getAllNotifications = async () => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}notification/get-restaurant-notification`,
      );
      if (response.data) {
        const data = _.orderBy(response.data?.data, ['createdAt'], ['desc']);
        setData(data);
      }

      setIsLoading(false);
      return true;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getAllNotifications().then(res => console.log('ALL NOTIFICATIONS: ', res));
  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={marginRight4}>
          <Ionicons name="notifications" size={26} color="#D2181B" />
          <Badge
            value={data.length > 0 ? data.length : 0}
            badgeStyle={bgRed}
            status="success"
            containerStyle={{position: 'absolute', top: -9, right: -6}}
          />
        </View>
      ),
    });
  }, [navigation, data]);

  const deleteNotification = async id => {
    try {
      const response = await axios.delete(
        `${apiBaseUrl}notification/remove-restaurant-notification${id}`,
      );
      if (response.data) {
        showToastWithGravityAndOffset('Notification deleted successfully!');
        getAllNotifications(userId).then(res =>
          console.log('ALL NOTIFICATIONS: ', res),
        );
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const renderItem =
    data.length > 0 ? (
      <SafeAreaView style={container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <RenderData data={data} deleteNotification={deleteNotification} />
        </ScrollView>
      </SafeAreaView>
    ) : (
      <View style={[marginTop2, paddingLeft5]}>
        <Text>No data found!</Text>
      </View>
    );

  return isLoading ? <Loader /> : renderItem;
};

export default Notifications;
