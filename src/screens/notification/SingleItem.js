import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import moment from "moment";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

const SingleItem = ({ item, index, deleteNotification }) => {
    const {
        f18,
        fw700,
        paddingBottom1,
        boxShadow,
        elevation3,
    } = globalStyles;

    const {
        notificationArea,
        notificationTime,
        circleIconArea,
    } = styles;

    const {
        _id,
        title,
        createdAt,
        description,
    } = item;

    return (
        <View style={notificationArea} key={index}>
            <View>
                <Text style={[f18, fw700, paddingBottom1]}>
                    {title}
                </Text>
                <Text style={notificationTime}>{description}</Text>
                <Text style={notificationTime}>
                    {moment(createdAt).format("DD/MM/YYYY hh:mm A")}
                </Text>
            </View>
            <TouchableOpacity
                style={[circleIconArea, boxShadow, elevation3]}
                onPress={() => deleteNotification(_id)}
            >
                <MaterialIcons name="delete" size={22} color="#D2181B" />
            </TouchableOpacity>
        </View>
    );
};

export default SingleItem;
