import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
        },
        circleIconArea: {
            backgroundColor: "#fff",
            width: 30,
            height: 30,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 15,
        },
        notificationArea: {
            flexDirection: "row",
            justifyContent: "space-between",
            paddingVertical: hp("2%"),
            paddingHorizontal: wp("5%"),
            backgroundColor: "#fff",
            elevation: 5,
            borderRadius: 10,
            marginHorizontal: wp("5%"),
            marginBottom: hp("2%"),
        },
        notificationTime: {
            color: "#979494",
        },
    },
);

export default styles;
