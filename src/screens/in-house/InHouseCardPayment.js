import React, {useEffect, useState} from 'react';

import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';

import initialMonthList from '../../json/monthList.json';
import CardPaymentForm from '../../shared-components/CardPaymentForm';

import {apiBaseUrl} from '../../config/index-example';
import {removeFromCart} from '../../store/actions/cart';
import {getOrderDetailsAndPrint} from '../../utilities/getOrderDetailsAndPrint';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

const currentYear = new Date().getFullYear().toString();

export default function InHouseCardPayment({navigation, route}) {
  const [monthList] = useState(initialMonthList);
  const [yearList, setYearList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const globalState = useSelector(state => state);
  const dispatch = useDispatch();

  const [state, setState] = useState({
    cardNumber: '',
    month: '01',
    year: currentYear,
    cvc: '',
  });

  const {cardNumber, month, year, cvc} = state;

  const {
    restaurant: {service_type, restaurantData},
  } = globalState;

  const {orderPayload} = route.params;

  function setYear() {
    let updateYearList = [];
    let currentYear = new Date().getFullYear();
    let maximumYear = new Date().getFullYear() + 10;

    for (currentYear; currentYear < maximumYear; currentYear++) {
      updateYearList.push(currentYear.toString());
    }

    setYearList(updateYearList);
  }

  useEffect(() => {
    setYear();
  }, []);

  const placeOrder = async () => {
    if (!cardNumber) {
      showToastWithGravityAndOffset('Card Number is required!');
      return false;
    }

    if (cardNumber.length !== 16) {
      showToastWithGravityAndOffset('Card Number must be 16 digit!');
      return false;
    }

    if (!month || !year) {
      showToastWithGravityAndOffset('Expiry Date is required!');
      return false;
    }

    if (!cvc) {
      showToastWithGravityAndOffset('CVC Number is required!');
      return false;
    }

    const paymentPayload = {
      cardNumber,
      cvc,
      exp_month: month,
      exp_year: year,
      total: orderPayload.totalPrice,
    };

    const {_id} = restaurantData;

    try {
      setIsLoading(true);

      let orderId;
      let apiEndPoint;

      if (['collection', 'delivery'].includes(service_type)) {
        apiEndPoint = 'order/create-inhouse-order';
      }

      if (!apiEndPoint) {
        showToastWithGravityAndOffset('Something went wrong! Please try again');
        setIsLoading(false);
        return false;
      }

      const orderResponse = await axios.post(
        `${apiBaseUrl}${apiEndPoint}`,
        orderPayload,
      );

      if (orderResponse.data) {
        removeFromCart(dispatch);
        orderId = orderResponse.data.data._id;

        try {
          const paymentResponse = await axios.post(
            `${apiBaseUrl}restaurant/${_id}/charge-by-card/${orderId}`,
            paymentPayload,
          );
          if (paymentResponse.data) {
            if (paymentResponse.data.success) {
              console.log('CARD PAYMENT: ', paymentResponse.data.success);

              setIsLoading(false);
              showToastWithGravityAndOffset('Order placed successfully!');
              getOrderDetailsAndPrint(orderId, restaurantData, true).then(res =>
                console.log('ORDER PRINT RESPONSE: ', !!res),
              );
              // navigation.navigate("InHouseOrder", {pending: false});
              navigation.navigate('RestaurantDetails');
            }
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          setIsLoading(false);
        }
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        if (error.response.data.error)
          showToastWithGravityAndOffset(error.response.data.error);
      }
      setIsLoading(false);
    }
  };

  return (
    <CardPaymentForm
      cardNumber={cardNumber}
      month={month}
      year={year}
      cvc={cvc}
      state={state}
      setState={setState}
      placeOrder={placeOrder}
      monthList={monthList}
      yearList={yearList}
      isLoading={isLoading}
    />
  );
}
