import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';

import axios from 'axios';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

import ButtonLoader from '../../../utilities/ButtonLoader';
import PaymentMethod from '../../../shared-components/PaymentMethod';
import OrderSummary from '../../../utilities/order-summary/OrderSummary';

import {apiBaseUrl} from '../../../config/index-example';
import {getTotalPrice} from '../payment/utility';
import {removeFromCart} from '../../../store/actions/cart';
import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

// Image
import cardImage from '../../../assets/images/card.png';
import {
  SET_CART_LIST,
  SET_ORDER_DETAILS,
  SET_SAVED_DINE_IN,
  SET_SERVICE,
} from '../../../store/types';

const InHouseDineInPayment = ({navigation, route}) => {
  let isCharged = 'false';
  //Getting Values from InHouseSumupPayment
  React.useEffect(async () => {
    let {uCode, date} = route?.params || {};
    console.log(`route paramas ucode ${uCode}`);
    console.log(`route paramas date ${date}`);
    if (uCode) {
      try {
        // let charge = true;
        let i = 0;
        const interval = setInterval(async () => {
          const response = await fetch(
            `https://food-apps-uk-default-rtdb.firebaseio.com/sumup/${uCode}/${date}.json`,
          );
          let data = await response.json();
          let charge = data.isCharged.toString();
          console.log(`This will run every second! ok ${charge}`);
          // charge=!charge
          if (charge === 'true') {
            isCharged = 'true';
            placeOrder('save').then(res =>
              console.log('ORDER RESPONSE: ', res),
            );

            clearInterval(interval);
          }
          if (i > 80) {
            clearInterval(interval);
            console.log('interval cleared!');
          }
          i++;
        }, 7000);
        return () => clearInterval(interval);
      } catch (e) {
        console.log(`Error for Fetching Data From Firebase `);
      }
    }
  }, [route.params]);

  const [paymentType, setPaymentType] = useState([
    {
      id: 1,
      value: 'Cash',
      name: 'Cash',
      img: null,
      icon: <FontAwesome5 name="cash-register" size={24} color="#524F4FFF" />,
      selected: false,
    },
    {
      id: 2,
      value: 'Card',
      name: 'Card',
      img: cardImage,
      selected: false,
    },
    {
      id: 3,
      value: 'Sumup',
      name: 'Sumup',
      img: null,
      icon: (
        <Image
          source={require('../../../../assets/img/sumup.png')}
          style={{height: 23, width: 40}}
        />
      ),
      selected: false,
    },
  ]);
  const [isLoading, setIsLoading] = useState(false);

  const globalState = useSelector(state => state);
  const dispatch = useDispatch();

  useEffect(() => {
    if (route.params) {
      const {orderDetailsId} = route.params;
      getOrderDetailsAndPrint(orderDetailsId).then(res => {
        console.log('ORDER DETAILS: ', !!res);

        const {cartItems, orderType} = res;

        if (orderType === 'Dine In') {
          dispatch({type: SET_SERVICE, service_type: 'dine_in'});
        }

        dispatch({type: SET_CART_LIST, payload: JSON.parse(cartItems)});
        dispatch({type: SET_SAVED_DINE_IN, payload: true});
        dispatch({type: SET_ORDER_DETAILS, payload: res});
      });
    }
  }, [route, dispatch]);

  const {
    restaurant: {cartList, restaurantData, service_type, orderDetails},
  } = globalState;

  const onRadioBtnClick = item => {
    const updateState = paymentType.map(value =>
      item.id === value.id
        ? {...value, selected: true}
        : {...value, selected: false},
    );

    setPaymentType(updateState);
  };

  const placeOrder = async () => {
    const paymentOptions = paymentType.find(
      item => item.selected === true && item,
    );

    if (!paymentOptions) {
      showToastWithGravityAndOffset('Payment option required!');
      return false;
    }

    const {_id, orderType} = orderDetails;

    const {serviceCharge} = restaurantData;

    const payload = {
      cartItems: JSON.stringify(cartList),
      orderType,
      subTotal: cartList.subTotal,
      totalPrice: getTotalPrice(service_type, cartList.subTotal, serviceCharge),
      paymentMethod: paymentOptions.value,
      screenType: 'inHouseDineInPayment',
    };

    if (paymentOptions.value === 'Card') {
      navigation.navigate('InHouseDineInCardPayment', {orderPayload: payload});
      return false;
    }

    if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
      navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
      return false;
    }

    try {
      setIsLoading(true);
      const dineInResponse = await axios.put(
        `${apiBaseUrl}order/update-order-inhouse/${_id}`,
        payload,
      );

      if (dineInResponse.data) {
        dispatch({type: SET_SAVED_DINE_IN, payload: false});
        dispatch({type: SET_ORDER_DETAILS, payload: null});
        setIsLoading(false);
        removeFromCart(dispatch);
        showToastWithGravityAndOffset('Order placed successfully!');
        getOrderDetailsAndPrint(
          dineInResponse.data.data._id,
          restaurantData,
          true,
        ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));

        // navigation.navigate('RestaurantDetails', {pending: false});
        removeFromCart(dispatch);
        navigation.navigate('RestaurantDetails', {pending: false});
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        if (error.response.data.error)
          showToastWithGravityAndOffset(error.response.data.error);
      }
      setIsLoading(false);
    }

    return true;
  };

  const {container} = styles;

  const {
    flex1,
    alignItemsBetween,
    paddingHorizontal5,
    paymentOptionArea,
    marginBottom5,
    continueButton,
    continueButtonArea,
    continueText,
    bgDisabledGrey,
  } = globalStyles;

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <OrderSummary />
      <SafeAreaView style={container}>
        <View style={[flex1, alignItemsBetween]}>
          <View style={[paddingHorizontal5, paymentOptionArea]}>
            <PaymentMethod
              paymentType={paymentType}
              onRadioBtnClick={onRadioBtnClick}
            />
          </View>
          <View style={[paddingHorizontal5, marginBottom5]}>
            <View style={continueButtonArea}>
              <TouchableOpacity
                style={[continueButton, !cartList && bgDisabledGrey]}
                onPress={() => {
                  if (!cartList) {
                    alert('Something went wrong!');
                    return;
                  }
                  placeOrder().then(res => console.log('DINE IN ORDER: ', res));
                }}>
                <Text style={continueText}>
                  {isLoading ? <ButtonLoader /> : 'Submit'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

export default InHouseDineInPayment;

// import React, {useEffect, useState} from 'react';
// import {
//   View,
//   Text,
//   SafeAreaView,
//   TouchableOpacity,
//   ScrollView,
//   Image,
// } from 'react-native';

// import axios from 'axios';
// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

// import {useDispatch, useSelector} from 'react-redux';

// import styles from './styles';
// import globalStyles from '../../../assets/styles/globalStyles';

// import ButtonLoader from '../../../utilities/ButtonLoader';
// import PaymentMethod from '../../../shared-components/PaymentMethod';
// import OrderSummary from '../../../utilities/order-summary/OrderSummary';

// import {apiBaseUrl} from '../../../config/index-example/index-example';
// import {getTotalPrice} from '../payment/utility';
// import {removeFromCart} from '../../../store/actions/cart';
// import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
// import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

// // Image
// import cardImage from '../../../assets/images/card.png';
// import {
//   SET_CART_LIST,
//   SET_ORDER_DETAILS,
//   SET_SAVED_DINE_IN,
//   SET_SERVICE,
// } from '../../../store/types';

// const InHouseDineInPayment = ({navigation, route}) => {
//   let isCharged = 'false';
//   //Getting Values from InHouseSumupPayment
//   React.useEffect(async () => {
//     let {uCode, date} = route?.params || {};
//     if (uCode) {
//       try {
//         // let charge = true;
//         let i = 0;
//         const interval = setInterval(async () => {
//           const response = await fetch(
//             `https://food-apps-uk-default-rtdb.firebaseio.com/sumup/${uCode}/${date}.json`,
//           );
//           let data = await response.json();
//           let charge = data.isCharged.toString();
//           console.log(`This will run every second! ok ${charge}`);
//           // charge=!charge
//           if (charge === 'true') {
//             isCharged = 'true';
//             placeOrder('save').then(res =>
//               console.log('ORDER RESPONSE: ', res),
//             );

//             clearInterval(interval);
//           }
//           if (i > 80) {
//             clearInterval(interval);
//             console.log('interval cleared!');
//           }
//           i++;
//         }, 7000);
//         return () => clearInterval(interval);
//       } catch (e) {
//         console.log(`Error for Fetching Data From Firebase `);
//       }
//     }
//   }, [route.params]);
//   const [paymentType, setPaymentType] = useState([
//     {
//       id: 1,
//       value: 'Cash',
//       name: 'Cash',
//       img: null,
//       icon: <FontAwesome5 name="cash-register" size={24} color="#524F4FFF" />,
//       selected: false,
//     },
//     {
//       id: 2,
//       value: 'Card',
//       name: 'Card',
//       img: cardImage,
//       selected: false,
//     },
//     {
//       id: 3,
//       value: 'Sumup',
//       name: 'Sumup',
//       img: null,
//       icon: (
//         <Image
//           source={require('../../../../assets/img/sumup.png')}
//           style={{height: 23, width: 40}}
//         />
//       ),
//       selected: false,
//     },
//   ]);
//   const [isLoading, setIsLoading] = useState(false);

//   const globalState = useSelector(state => state);
//   const dispatch = useDispatch();

//   useEffect(() => {
//     if (route.params) {
//       const {orderDetailsId} = route.params;
//       getOrderDetailsAndPrint(orderDetailsId).then(res => {
//         console.log('ORDER DETAILS: ', !!res);

//         const {cartItems, orderType} = res;

//         if (orderType === 'Dine In') {
//           dispatch({type: SET_SERVICE, service_type: 'dine_in'});
//         }

//         dispatch({type: SET_CART_LIST, payload: JSON.parse(cartItems)});
//         dispatch({type: SET_SAVED_DINE_IN, payload: true});
//         dispatch({type: SET_ORDER_DETAILS, payload: res});
//       });
//     }
//   }, [route]);

//   const {
//     restaurant: {cartList, restaurantData, service_type, orderDetails},
//   } = globalState;

//   const onRadioBtnClick = item => {
//     const updateState = paymentType.map(value =>
//       item.id === value.id
//         ? {...value, selected: true}
//         : {...value, selected: false},
//     );

//     setPaymentType(updateState);
//   };

//   const placeOrder = async () => {
//     const paymentOptions = paymentType.find(
//       item => item.selected === true && item,
//     );

//     if (!paymentOptions) {
//       showToastWithGravityAndOffset('Payment option required!');
//       return false;
//     }

//     const {_id, orderType} = orderDetails;

//     const {serviceCharge} = restaurantData;

//     const payload = {
//       cartItems: JSON.stringify(cartList),
//       orderType,
//       subTotal: cartList.subTotal,
//       totalPrice: getTotalPrice(service_type, cartList.subTotal, serviceCharge),
//       paymentMethod: paymentOptions.value,
//       screenType: 'inHouseDineInPayment',
//     };

//     if (paymentOptions.value === 'Card') {
//       navigation.navigate('InHouseDineInCardPayment', {orderPayload: payload});
//       return false;
//     }
//     if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
//       navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
//       return false;
//     }

//     try {
//       setIsLoading(true);
//       const dineInResponse = await axios.put(
//         `${apiBaseUrl}order/update-order-inhouse/${_id}`,
//         payload,
//       );

//       if (dineInResponse.data) {
//         setIsLoading(false);
//         removeFromCart(dispatch);
//         showToastWithGravityAndOffset('Order placed successfully!');
//         getOrderDetailsAndPrint(
//           dineInResponse.data.data._id,
//           restaurantData,
//           true,
//         ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));

//         navigation.navigate('InHouseOrder', {pending: false});
//       }
//     } catch (error) {
//       if (error.response.data) {
//         console.log(error.response.data);
//         if (error.response.data.error)
//           showToastWithGravityAndOffset(error.response.data.error);
//       }
//       setIsLoading(false);
//     }

//     return true;
//   };

//   const {container} = styles;

//   const {
//     flex1,
//     alignItemsBetween,
//     paddingHorizontal5,
//     paymentOptionArea,
//     marginBottom5,
//     continueButton,
//     continueButtonArea,
//     continueText,
//     bgDisabledGrey,
//   } = globalStyles;

//   return (
//     <ScrollView showsVerticalScrollIndicator={false}>
//       <OrderSummary />
//       <SafeAreaView style={container}>
//         <View style={[flex1, alignItemsBetween]}>
//           <View style={[paddingHorizontal5, paymentOptionArea]}>
//             <PaymentMethod
//               paymentType={paymentType}
//               onRadioBtnClick={onRadioBtnClick}
//             />
//           </View>
//           <View style={[paddingHorizontal5, marginBottom5]}>
//             <View style={continueButtonArea}>
//               <TouchableOpacity
//                 style={[continueButton, !cartList && bgDisabledGrey]}
//                 onPress={() => {
//                   if (!cartList) {
//                     alert('Something went wrong!');
//                     return;
//                   }
//                   placeOrder().then(res => console.log('DINE IN ORDER: ', res));
//                 }}>
//                 <Text style={continueText}>
//                   {isLoading ? <ButtonLoader /> : 'Submit'}
//                 </Text>
//               </TouchableOpacity>
//             </View>
//           </View>
//         </View>
//       </SafeAreaView>
//     </ScrollView>
//   );
// };

// export default InHouseDineInPayment;
