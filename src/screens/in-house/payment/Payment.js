import React, {useEffect, useState} from 'react';
import {SafeAreaView, ScrollView} from 'react-native';

import axios from 'axios';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {useDispatch, useSelector} from 'react-redux';
import styles from './styles';
// const {container, leftView, rightView} = styles;

import {apiBaseUrl} from '../../../config/index-example';
import {removeFromCart} from '../../../store/actions/cart';
import {validateInputField, getTotalPrice} from './utility';
import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

// Component
import ButtonInput from './ButtonInput';
import AddressInput from './AddressInput';
import PhoneNumberInput from './PhoneNumberInput';
import TableNumberInput from './TableNumberInput';
import PaymentTypeInput from './PaymentTypeInput';
import TableNumberModal from './TableNumberModal';
import CustomerNameInput from './CustomerNameInput';
import OrderSummary from '../../../utilities/order-summary/OrderSummary';
import {Image} from 'react-native-elements';

// Images
import cardImage from '../../../../assets/img/card.png';
import {View} from 'native-base';

const Payment = ({navigation, route}) => {
  let isCharged = 'false';
  //Getting Values from InHouseSumupPayment
  let interval;
  React.useEffect(async () => {
    let {uCode, date} = route?.params || {};
    if (uCode) {
      try {
        // let charge = true;
        let i = 0;
        interval = setInterval(async () => {
          const response = await fetch(
            `https://food-apps-uk-default-rtdb.firebaseio.com/sumup/${uCode}/${date}.json`,
          );
          let data = await response.json();
          let charge = data.isCharged.toString();
          console.log(`This will run every second! ok ${charge}`);
          // charge=!charge
          if (charge === 'true') {
            isCharged = 'true';
            placeOrder('save').then(res =>
              console.log('ORDER RESPONSE: ', res),
            );

            clearInterval(interval);
          }
          if (i > 80) {
            clearInterval(interval);
            console.log('interval cleared!');
          }
          i++;
        }, 7000);
        return () => clearInterval(interval);
      } catch (e) {
        console.log(`Error for Fetching Data From Firebase `);
      }
    }
  }, [route.params]);

  const [isLoading, setIsLoading] = useState(false);
  const [paymentType, setPaymentType] = useState([
    {
      id: 1,
      value: 'Cash',
      name: 'Cash',
      img: null,
      icon: <FontAwesome5 name="cash-register" size={24} color="#524F4FFF" />,
      selected: false,
    },
    {
      id: 2,
      value: 'Card',
      name: 'Card',
      img: cardImage,
      selected: false,
    },
    {
      id: 3,
      value: 'Sumup',
      name: 'Sumup',
      img: null,
      icon: (
        <Image
          source={require('../../../../assets/img/sumup.png')}
          style={{height: 23, width: 40}}
        />
      ),
      selected: false,
    },
  ]);
  const [contactInputEditable, setContactInputEditable] = useState(false);
  const [customerInputEditable, setCustomerInputEditable] = useState(false);
  const [deliveryAddressInputEditable, setDeliveryAddressInputEditable] =
    useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [state, setState] = useState({
    customerName: '',
    address: '',
    postCode: '',
    phoneNumber: '',
    tableNo: '',
    deliveryCharge: 0,
  });

  const {restaurant} = useSelector(state => state);
  const dispatch = useDispatch();

  const {service_type, cartList, restaurantData, savedDineIn, orderDetails} =
    restaurant;

  const {
    customerName,
    address,
    postCode,
    phoneNumber,
    tableNo,
    deliveryCharge,
  } = state;

  useEffect(() => {
    if (savedDineIn && orderDetails) {
      const {customerName, mobileNo, tableNo} = orderDetails;

      setState({
        customerName,
        address: '',
        postCode: '',
        phoneNumber: mobileNo,
        tableNo,
      });
    }
  }, [savedDineIn, orderDetails]);

  const placeOrder = async (buttonType = '') => {
    const paymentOptions = paymentType.find(item => item.selected === true);

    const {_id, coupon, uniId, isRiderService, discount, serviceCharge} =
      restaurantData;

    const validationObj = {
      customerName,
      phoneNumber,
      service_type,
      tableNo,
      address,
      postCode,
      paymentOptions,
    };

    if (!validateInputField(validationObj)) return false;

    let payload = {
      restaurant: _id,
      restaurantUniId: uniId,
      coupon,
      customerName,
      mobileNo: phoneNumber,
      subTotal: cartList.subTotal,
      shippingFee: 0,
      screenType: 'paymentScreen',
      totalPrice: getTotalPrice(
        service_type,
        cartList.subTotal,
        serviceCharge,
        deliveryCharge,
      ),
      paymentMethod: paymentOptions ? paymentOptions.value : '',
      kitchenNotes: cartList.note,
      riderService: isRiderService,
      cartItems: JSON.stringify(cartList),
      postCode,
      fromRestaurant: true,
    };

    switch (service_type) {
      case 'dine_in':
        payload.orderType = 'Dine In';
        payload.tableNo = parseInt(tableNo);
        payload.serviceCharge = serviceCharge;

        delete payload.postCode;
        delete payload.shippingFee;
        delete payload.riderService;

        if (buttonType === 'serve') {
          navigation.navigate('InHouseDineInPayment');
          return false;
        }

        payload.paymentMethod = 'Counter';
        payload.paymentStatus = 'Pending';

        try {
          // setIsLoading(true);

          if (savedDineIn && orderDetails) {
            const apiEndPoint = `order/update-inhouse-pendingOrder/${orderDetails._id}`;
            const dineInResponse = await axios.put(
              `${apiBaseUrl}${apiEndPoint}`,
              cartList,
            );

            if (dineInResponse.data) {
              removeFromCart(dispatch);
              // setIsLoading(false);
              showToastWithGravityAndOffset('Order updated successfully!');
              navigation.navigate('InHouseOrder', {pending: true});
            }
          } else {
            const dineInResponse = await axios.post(
              `${apiBaseUrl}order/create-inhouse-dine-in`,
              payload,
            );

            if (dineInResponse.data) {
              removeFromCart(dispatch);
              // setIsLoading(false);
              showToastWithGravityAndOffset('Order placed successfully!');
              getOrderDetailsAndPrint(
                dineInResponse.data.data._id,
                restaurantData,
                true,
              ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));
              navigation.navigate('RestaurantDetails', {pending: true});
            }
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          // setIsLoading(false);
        }
        break;
      case 'collection':
        payload.orderType = 'Collection';

        if (paymentOptions.value === 'Card') {
          navigation.navigate('InHouseCardPayment', {orderPayload: payload});
          return false;
        }
        if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
          navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
          return false;
        }

        try {
          // setIsLoading(true);
          const collectionResponse = await axios.post(
            `${apiBaseUrl}order/create-inhouse-order`,
            payload,
          );

          if (collectionResponse.data) {
            removeFromCart(dispatch);
            // setIsLoading(false);
            showToastWithGravityAndOffset('Order placed successfully!');
            getOrderDetailsAndPrint(
              collectionResponse.data.data._id,
              restaurantData,
              true,
            ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));
            navigation.navigate('RestaurantDetails', {pending: false});
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          // setIsLoading(false);
        }
        break;
      case 'delivery':
        payload.deliveredAt = address;
        payload.orderType = 'Home Delivery';
        payload.deliveryCharge = deliveryCharge;
        payload.totalPrice = getTotalPrice(
          service_type,
          cartList.subTotal,
          serviceCharge,
          deliveryCharge,
        );

        if (paymentOptions.value === 'Card') {
          navigation.navigate('InHouseCardPayment', {orderPayload: payload});
          return false;
        }
        if (paymentOptions.value === 'Sumup' && isCharged === 'false') {
          navigation.navigate('InHouseSumupPayment', {orderPayload: payload});
          return false;
        }

        try {
          // setIsLoading(true);
          const deliveryResponse = await axios.post(
            `${apiBaseUrl}order/create-inhouse-order`,
            payload,
          );

          if (deliveryResponse.data) {
            removeFromCart(dispatch);
            // setIsLoading(false);
            showToastWithGravityAndOffset('Order placed successfully!');
            getOrderDetailsAndPrint(
              deliveryResponse.data.data._id,
              restaurantData,
              true,
            ).then(res => console.log('ORDER PRINT RESPONSE: ', !!res));
            // navigation.navigate('InHouseOrder', {pending: false});
            navigation.navigate('RestaurantDetails', {pending: false});
          }
        } catch (error) {
          if (error.response.data) {
            console.log(error.response.data);
            if (error.response.data.error)
              showToastWithGravityAndOffset(error.response.data.error);
          }
          // setIsLoading(false);
        }
        break;
      default:
        return true;
    }

    return true;
  };

  const onRadioBtnClick = item => {
    const updateState = paymentType.map(value =>
      item.id === value.id
        ? {...value, selected: true}
        : {...value, selected: false},
    );
    setPaymentType(updateState);
  };

  const isDineIn = service_type === 'dine_in';

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={styles.container}>
          {/* <CustomerNameInput
            state={state}
            setState={setState}
            isDineIn={isDineIn}
            customerName={customerName}
            customerInputEditable={customerInputEditable}
            setCustomerInputEditable={setCustomerInputEditable}
          /> */}

          <View style={styles.rightView}>
            <PhoneNumberInput
              state={state}
              setState={setState}
              phoneNumber={phoneNumber}
              isDineIn={isDineIn}
              contactInputEditable={contactInputEditable}
              setContactInputEditable={setContactInputEditable}
            />
          </View>
          <View style={styles.rightView}>
            {service_type === 'dine_in' && (
              <TableNumberInput
                tableNo={tableNo}
                state={state}
                setState={setState}
                // isModalVisible={isModalVisible}
                // setModalVisible={setModalVisible}
                savedDineIn={savedDineIn}
              />
            )}
            <View>
              {service_type === 'collection' && (
                <CustomerNameInput
                  state={state}
                  setState={setState}
                  isDineIn={isDineIn}
                  customerName={customerName}
                  customerInputEditable={customerInputEditable}
                  setCustomerInputEditable={setCustomerInputEditable}
                />
              )}
            </View>
            <View>
              {service_type === 'delivery' && (
                <CustomerNameInput
                  state={state}
                  setState={setState}
                  isDineIn={isDineIn}
                  customerName={customerName}
                  customerInputEditable={customerInputEditable}
                  setCustomerInputEditable={setCustomerInputEditable}
                />
              )}
            </View>
          </View>
        </View>

        {service_type === 'delivery' && (
          <AddressInput
            state={state}
            setState={setState}
            address={address}
            postCode={postCode}
            restaurantData={restaurantData}
            deliveryAddressInputEditable={deliveryAddressInputEditable}
            setDeliveryAddressInputEditable={setDeliveryAddressInputEditable}
          />
        )}

        <OrderSummary deliveryCharge={deliveryCharge} />

        {service_type !== 'dine_in' && (
          <PaymentTypeInput
            paymentType={paymentType}
            onRadioBtnClick={onRadioBtnClick}
          />
        )}

        <ButtonInput
          placeOrder={placeOrder}
          isLoading={isLoading}
          service_type={service_type}
          savedDineIn={savedDineIn}
        />
      </ScrollView>

      {isModalVisible && (
        <TableNumberModal
          isModalVisible={isModalVisible}
          setModalVisible={setModalVisible}
          state={state}
          setState={setState}
          table={restaurantData.table}
        />
      )}
    </SafeAreaView>
  );
};

export default Payment;
