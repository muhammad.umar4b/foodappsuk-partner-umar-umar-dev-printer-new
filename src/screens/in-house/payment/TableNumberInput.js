import React from 'react';
import {Image, Pressable, Text, TextInput, View} from 'react-native';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

// Image
import tableBookingImage from '../../../../assets/table-booking.png';

const TableNumberInput = props => {
  const {
    tableNo,
    savedDineIn,
    isModalVisible,
    setModalVisible,
    state,
    setState,
  } = props;

  const {
    card,
    boxShadow,
    marginTop3,
    cardHeader,
    elevation5,
    marginLeft06,
    paddingTop3,
    flexDirectionRow,
    paddingHorizontal5,
  } = globalStyles;

  const {inputField, circleIconArea, cardHeaderLabel, bookingIconImage} =
    styles;
  const tableNumber = tableNo.toString();

  return (
    <View style={paddingHorizontal5}>
      <View style={[card, boxShadow, marginTop3]}>
        <View style={cardHeader}>
          <View style={flexDirectionRow}>
            <View style={[circleIconArea, boxShadow, elevation5, marginLeft06]}>
              <Image source={tableBookingImage} style={bookingIconImage} />
            </View>
            <Text style={cardHeaderLabel}>Table Number</Text>
          </View>
        </View>
        <View style={paddingTop3}>
          {/*<Pressable onPress={() => {
                        if (savedDineIn) {
                            alert("Table Number is already assigned!");
                            return;
                        }
                        setModalVisible(!isModalVisible);
                    }}>*/}
          <TextInput
            value={tableNumber}
            editable={!savedDineIn}
            style={inputField}
            keyboardType={'default'}
            placeholder={'Enter Table No (Required)'}
            onChangeText={value => setState({...state, tableNo: value})}
          />
        </View>
      </View>
    </View>
  );
};

export default TableNumberInput;
