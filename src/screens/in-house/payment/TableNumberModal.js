import React from "react";
import {ScrollView, Text, TouchableOpacity, View} from "react-native";

import Modal from "react-native-modal";

import styles from "./styles";

const TableNumberModal = (props) => {
    const {
        isModalVisible,
        setModalVisible,
        state,
        setState,
        table
    } = props;

    const {
        modalView,
        modalBody,
        modalFooter,
        monthArea,
        monthAreaContent,
        monthAreaContentBlock,
        monthAreaContentText,
        monthAreaContentTextActive,
        monthAreaHeaderText,
        closeButton,
        continueText,
    } = styles;

    const {
        tableNo
    } = state;

    return (
        <Modal
            isVisible={isModalVisible}
            style={modalView}>
            <View>
                <View style={modalBody}>
                    <View style={monthArea}>
                        <Text style={monthAreaHeaderText}>Table Number</Text>
                        <ScrollView style={monthAreaContent}>
                            {(table && table.length > 0) ?
                                table.map((item, index) => (
                                    !item['isBooking'] && <TouchableOpacity
                                        style={monthAreaContentBlock} key={index}
                                        onPress={() => setState({...state, tableNo: item['No']})}
                                    >
                                        <Text
                                            style={[monthAreaContentText, tableNo === item['No'] && monthAreaContentTextActive]}>
                                            {item['No']}
                                        </Text>
                                    </TouchableOpacity>
                                ))
                                :
                                <Text>No Table Found!</Text>
                            }
                        </ScrollView>
                    </View>
                </View>
                <View style={modalFooter}>
                    <TouchableOpacity
                        style={closeButton}
                        onPress={() => {
                            if (!tableNo) {
                                alert('Table Number is required!');
                                return false;
                            }
                            setModalVisible(!isModalVisible);
                        }}
                    >
                        <Text style={continueText}>Ok</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

export default TableNumberModal;
