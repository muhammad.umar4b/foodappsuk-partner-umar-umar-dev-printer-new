import {showToastWithGravityAndOffset} from '../../../shared-components/ToastMessage';

export const validateInputField = obj => {
  const {service_type, tableNo, address, postCode, paymentOptions} = obj;

  if (service_type === 'dine_in') {
    if (!tableNo) {
      showToastWithGravityAndOffset('Table Number field is required!');
      return false;
    }
  }

  if (service_type === 'delivery') {
    if (!address) {
      showToastWithGravityAndOffset('Address field is required!');
      return false;
    }

    if (!postCode) {
      showToastWithGravityAndOffset('Post Code field is required!');
      return false;
    }
  }

  if (service_type !== 'dine_in') {
    if (!paymentOptions) {
      showToastWithGravityAndOffset('Payment option required!');
      return false;
    }
  }

  return true;
};

export const getTotalPrice = (
  service_type,
  price,
  serviceCharge,
  deliveryCharge = 0,
  partnerFACharge = 0,
) => {
  let totalPrice = price;

  if (service_type === 'dine_in') {
    totalPrice = parseFloat(parseFloat(price + serviceCharge).toFixed(2));
  }

  if (service_type === 'delivery') {
    totalPrice = parseFloat(
      (totalPrice + parseFloat(deliveryCharge.toString())).toFixed(2),
    );
  }

  if (service_type !== 'dine_in') totalPrice += partnerFACharge;

  return totalPrice;
};
