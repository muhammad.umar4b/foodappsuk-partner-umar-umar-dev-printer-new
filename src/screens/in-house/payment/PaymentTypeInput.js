import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import globalStyles from "../../../assets/styles/globalStyles";

const PaymentTypeInput = (props) => {
    const {
        paymentType,
        onRadioBtnClick,
    } = props;

    const {
        radioButtonArea,
        paddingTop3,
        radioButtonContainer,
        radioButton,
        radioButtonIcon,
        radioButtonContentArea,
        marginRight1,
        radioButtonImage,
        radioButtonText,
        paddingHorizontal5,
        marginLeft3
    } = globalStyles;

    return (
        <View style={paddingHorizontal5}>
            <View style={[radioButtonArea, paddingTop3]}>
                {paymentType.map((item, key) => (
                    <View style={radioButtonContainer} key={item.id}>
                        <TouchableOpacity onPress={() => onRadioBtnClick(item)}
                                          style={[radioButton, key > 0 ? marginLeft3 : {}]}>
                            {item.selected ? <View style={radioButtonIcon} /> : null}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => onRadioBtnClick(item)}
                                          style={radioButtonContentArea}>
                            {item.img &&
                                <Image style={radioButtonImage} source={item.img} />
                            }
                            {item.icon &&
                                <View style={marginRight1}>{item.icon}</View>
                            }
                            <Text style={radioButtonText}>{item.name}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        </View>
    );
};

export default PaymentTypeInput;
