import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';

import _ from 'lodash';
import axios from 'axios';
import moment from 'moment';
import Feather from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import Loader from '../../utilities/Loader';
import globalStyles from '../../assets/styles/globalStyles';

import {apiBaseUrl} from '../../config/index-example';
import {DINE_IN_DELIVERY_TIME} from '../../utilities/const';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

const DineInOrderItem = props => {
  const {navigation, index, item, setStatusBgColor, getStatusButtonText} =
    props;

  const {
    card,
    boxShadow,
    marginTop2,
    flexDirectionRow,
    justifyBetween,
    marginBottom1,
    textGrey,
    marginLeft2,
    paddingTop1,
    paddingBottom2,
    textCapitalize,
  } = globalStyles;

  const {statusButton, statusButtonText, totalAmountArea} = styles;

  return (
    <View style={[card, boxShadow, marginTop2]} key={index}>
      <View style={[flexDirectionRow, justifyBetween, marginBottom1]}>
        <View style={[flexDirectionRow, {height: 18}]}>
          <Text style={textGrey}>Table No : {item.tableNo}</Text>
          <View
            style={[
              statusButton,
              setStatusBgColor(item['createdAt'], DINE_IN_DELIVERY_TIME),
              marginLeft2,
            ]}>
            <Text style={statusButtonText}>
              {getStatusButtonText(item['createdAt'], DINE_IN_DELIVERY_TIME)}
            </Text>
          </View>
        </View>
      </View>

      <TouchableOpacity
        onPress={() =>
          navigation.navigate('InHouseDineInOrderDetails', {orderDetails: item})
        }>
        <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
          <Text>Order Date</Text>
          <Text>
            {moment(item['updatedAt']).format('DD MMM YYYY, hh:mm A') || 'N/A'}
          </Text>
        </View>

        <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
          <Text>Time</Text>
          <Text>{DINE_IN_DELIVERY_TIME}</Text>
        </View>

        <View
          style={[
            flexDirectionRow,
            justifyBetween,
            paddingTop1,
            paddingBottom2,
          ]}>
          <Text>Payment</Text>
          <Text style={textCapitalize}>{item.paymentMethod || 'N/A'}</Text>
        </View>

        <View style={totalAmountArea}>
          <Text>Total Price</Text>
          <Text>£{parseFloat(item.totalPrice).toFixed(2)}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const DineInOrderNotServedItem = props => {
  const {navigation, index, item, userId, restaurantInfo, placeOrder} = props;

  const {
    card,
    boxShadow,
    marginTop2,
    flexDirectionRow,
    justifyBetween,
    marginBottom1,
    textGrey,
    paddingTop1,
    paddingBottom2,
    textCapitalize,
    bgCyan,
    marginLeft2,
    elevation3,
    bgRed,
  } = globalStyles;

  const {statusButton, statusButtonText, totalAmountArea, circleIconArea} =
    styles;

  return (
    <View style={[card, boxShadow, marginTop2]} key={index}>
      <View style={[flexDirectionRow, justifyBetween, marginBottom1]}>
        <View style={[flexDirectionRow, {height: 18}]}>
          <Text style={textGrey}>Table No : {item.tableNo}</Text>
          <View style={[statusButton, bgCyan, marginLeft2]}>
            <Text style={statusButtonText}>Saved</Text>
          </View>
          <TouchableOpacity
            style={[statusButton, bgRed, marginLeft2]}
            onPress={() => placeOrder()}>
            <Text style={statusButtonText}>Serve</Text>
          </TouchableOpacity>
        </View>

        <View style={[flexDirectionRow]}>
          <TouchableOpacity
            style={[circleIconArea, boxShadow, elevation3]}
            onPress={() =>
              navigation.navigate('RestaurantDetails', {
                userId,
                restaurantId: restaurantInfo,
              })
            }>
            <Feather name="edit" size={22} color="#D2181B" />
          </TouchableOpacity>
          {/*<TouchableOpacity
                        style={[circleIconArea, boxShadow, elevation3, marginLeft2]}
                        onPress={() => placeOrder()}
                    >
                        <MaterialCommunityIcons name="delete" size={22} color="#D2181B" />
                    </TouchableOpacity>*/}
        </View>
      </View>

      <TouchableOpacity
        onPress={() =>
          navigation.navigate('InHouseDineInOrderDetails', {orderDetails: item})
        }>
        <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
          <Text>Order Date</Text>
          <Text>{moment().format('DD MMM YYYY, hh:mm A') || 'N/A'}</Text>
        </View>

        <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
          <Text>Time</Text>
          <Text>{DINE_IN_DELIVERY_TIME}</Text>
        </View>

        <View
          style={[
            flexDirectionRow,
            justifyBetween,
            paddingTop1,
            paddingBottom2,
          ]}>
          <Text>Payment</Text>
          <Text style={textCapitalize}>{item.paymentMethod || 'N/A'}</Text>
        </View>

        <View style={totalAmountArea}>
          <Text>Total Price</Text>
          <Text>£{item.totalPrice}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const DineInOrder = ({navigation, route}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [cartList, setCartList] = useState([]);
  const dispatch = useDispatch();
  const state = useSelector(state => state);
  const {
    restaurant: {restaurantInfo, inHouseDineOrderList},
    auth: {userId},
  } = state;

  console.log(JSON.stringify(cartList), 'cartList');

  const getMyOrders = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}order/fetch-dine-in-by-res/${id}`,
      );
      if (response.data) {
        const data = _.orderBy(response.data.data, ['createdAt'], ['desc']);
        setData(data);
        setIsLoading(false);
        return true;
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const getRestaurantCart = async id => {
    try {
      const response = await axios.get(
        `${apiBaseUrl}food/fetch-restaurant-cart/${userId}?restaurant=${id}`,
      );
      return response.data;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const setRestaurantCart = data => {
    setCartList(data);
    return true;
  };

  useEffect(() => {
    getMyOrders(restaurantInfo).then(res =>
      console.log('IN HOUSE DINE IN ORDER: ', res),
    );
    getRestaurantCart(restaurantInfo).then(res => setRestaurantCart(res.data));
  }, [restaurantInfo, route.params]);

  const removeFromCart = () => {
    cartList.forEach(item => {
      axios
        .post(`${apiBaseUrl}food/sub-from-cart/${item['_id']}`)
        .then(res => console.log(res.data.data, 'res.data.data'))
        .catch(err => console.log(err.response));
    });
  };

  const placeOrder = async () => {
    try {
      const dineInResponse = await axios.post(
        `${apiBaseUrl}order/create-dine-in`,
        inHouseDineOrderList,
      );
      if (dineInResponse.data) {
        removeFromCart();
        showToastWithGravityAndOffset('Order placed successfully!');
        getMyOrders(restaurantInfo).then(res =>
          console.log('IN HOUSE DINE IN ORDER: ', res),
        );
        dispatch({type: 'SET_IN_HOUSE_DINE_IN_ORDER', payload: null});
      }
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
      }
    }
  };

  const setStatusBgColor = (createdAt, time) => {
    const statusText = getStatusButtonText(createdAt, time);
    if (statusText === 'Received') return globalStyles.bgSuccess;
    if (statusText === 'Processing') return globalStyles.bgWarning;
    if (statusText === 'Served') return globalStyles.bgRed;
  };

  const getStatusButtonText = (createdAt, time) => {
    const a = moment(new Date());
    const b = moment(createdAt);
    const diff = a.diff(b, 'minutes');

    if (time) {
      if (diff > 1 && diff < time) {
        return 'Processing';
      } else if (diff > time) {
        return 'Served';
      } else {
        return 'Received';
      }
    } else {
      if (diff > 1) {
        return 'Processing';
      } else {
        return 'Received';
      }
    }
  };

  const noOrderYet = !inHouseDineOrderList ? (
    <Text style={globalStyles.paddingTop1}>No Order Yet!</Text>
  ) : (
    ''
  );

  const renderOrder =
    data.length > 0
      ? data.map((item, index) => (
          <DineInOrderItem
            navigation={navigation}
            index={index}
            item={item}
            setStatusBgColor={setStatusBgColor}
            getStatusButtonText={getStatusButtonText}
            key={index}
          />
        ))
      : noOrderYet;

  const renderNotServedOrder = inHouseDineOrderList && (
    <DineInOrderNotServedItem
      navigation={navigation}
      item={inHouseDineOrderList}
      userId={userId}
      restaurantInfo={restaurantInfo}
      placeOrder={placeOrder}
    />
  );

  return isLoading ? (
    <Loader />
  ) : (
    <SafeAreaView style={globalStyles.flex1}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={[
            globalStyles.paddingHorizontal5,
            globalStyles.paddingBottom3,
          ]}>
          {renderNotServedOrder}
          {renderOrder}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  statusButton: {
    borderRadius: 5,
  },
  statusButtonText: {
    paddingVertical: hp('0.2%'),
    paddingHorizontal: wp('1.5%'),
    textTransform: 'uppercase',
    fontWeight: '700',
    color: '#fff',
  },
  totalAmountArea: {
    borderTopColor: '#b4b4b4',
    borderTopWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: hp('1%'),
  },
  loadingArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  continueButton: {
    backgroundColor: '#D2181B',
    paddingVertical: hp('1%'),
    paddingHorizontal: wp('3%'),
    borderRadius: 8,
  },
  continueText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
  },
  circleIconArea: {
    backgroundColor: '#fff',
    width: 30,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
});

export default DineInOrder;
