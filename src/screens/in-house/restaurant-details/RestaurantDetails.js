import React, {useEffect, useState, useLayoutEffect} from 'react';
import {Text, ScrollView, TouchableOpacity} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import globalStyles from '../../../assets/styles/globalStyles';

import {getWeekDay} from '../../../utilities/global';
import {getOrderDetailsAndPrint} from '../../../utilities/getOrderDetailsAndPrint';
import {
  SET_CART_LIST,
  SET_SERVICE,
  SET_SAVED_DINE_IN,
  SET_ORDER_DETAILS,
} from '../../../store/types';

import {getRestaurantInfo} from '../../../store/actions/restaurant';

// Component
import BasketArea from './BasketArea';
import CategoryArea from './CategoryArea';
import Loader from '../../../utilities/Loader';
import ServiceTypeArea from './ServiceTypeArea';
import AddToCartModal from './add-to-cart-modal/AddToCartModal';
import SpecialOfferModal from './special-offer-modal/SpecialOfferModal';

const RestaurantDetails = ({navigation, route}) => {
  const [isModalVisible, setModalVisible] = useState(false);
  const [modalState, setModalState] = useState(null);
  const [categoryList, setCategoryList] = useState([]);
  const [restaurantInfo, setRestaurantInfo] = useState(null);
  const [restaurantInfoLoading, setRestaurantInfoLoading] = useState(true);
  const [serviceListArea, setServiceListArea] = useState([
    {
      key: 'dine_in',
      name: 'Dine In',
      isActive: true,
    },
    {
      key: 'collection',
      name: 'Collection',
      isActive: false,
    },
    {
      key: 'delivery',
      name: 'Delivery',
      isActive: false,
    },
  ]);
  const [isViewBasket, setIsViewBasket] = useState(false);
  const [state, setState] = useState({
    count: 1,
    isAdd: false,
  });
  const [isRestaurantOn, setIsRestaurantOn] = useState(true);
  const [isSpecialOfferModalVisible, setIsSpecialOfferModalVisible] =
    useState(false);
  const [specialOfferModalState, setSpecialOfferModalState] = useState(null);
  const [activeCategory, setActiveCategory] = useState(null);
  const [foodItemLoading, setFoodItemLoading] = useState(false);
  const [foodItemList, setFoodItemList] = useState([]);
  const [categoryListLoading, setCategoryListLoading] = useState(false);

  const dispatch = useDispatch();
  const {
    restaurant: {service_type, cartList, categories, foods, restaurantData},
  } = useSelector(state => state);

  const {count, isAdd} = state;

  const {container, paddingHorizontal0, f17, textWhite} = globalStyles;

  const {menuBarServiceArea} = styles;

  useLayoutEffect(() => {
    // const activeService = serviceListArea.find(item => item.isActive && item);
    navigation.setOptions({
      title: restaurantInfo ? restaurantInfo.name : 'Restaurant',
      headerRight: () => (
        <TouchableOpacity
          style={menuBarServiceArea}
          onPress={() => navigation.navigate('InHouseOrder', {pending: true})}>
          <Text style={[f17, textWhite]}>Orders</Text>
        </TouchableOpacity>
      ),
    });
  }, [navigation, serviceListArea, restaurantInfo]);

  useEffect(() => {
    if (restaurantData) {
      const {
        restaurantTiming: {
          restaurantTiming: {available},
        },
      } = restaurantData;

      const weekDay = getWeekDay();
      const restaurantTime = available.find(item => item['day'] === weekDay);

      setRestaurantInfo(restaurantData);
      setIsRestaurantOn(restaurantTime ? restaurantTime.isOpen : false);
      setRestaurantInfoLoading(false);
    }
  }, [restaurantData]);

  useEffect(() => {
    if (route.params && route.params.orderDetailsId) {
      getOrderDetailsAndPrint(route.params.orderDetailsId).then(res => {
        console.log('ORDER DETAILS: ', !!res);

        const {cartItems, orderType} = res;

        if (orderType === 'Dine In') {
          const updateServiceListArea = serviceListArea.map(item => {
            item.isActive = item.key === 'dine_in';
            return item;
          });

          setServiceListArea(updateServiceListArea);
          dispatch({type: SET_SERVICE, service_type: 'dine_in'});
        }

        dispatch({type: SET_CART_LIST, payload: JSON.parse(cartItems)});
        dispatch({type: SET_SAVED_DINE_IN, payload: true});
        dispatch({type: SET_ORDER_DETAILS, payload: res});
      });
    } else {
      const updateServiceListArea = serviceListArea.map(item => {
        item.isActive = item.key === service_type;
        return item;
      });

      setServiceListArea(updateServiceListArea);

      dispatch({type: SET_SERVICE, service_type});
      dispatch({type: SET_CART_LIST, payload: null});
      dispatch({type: SET_SAVED_DINE_IN, payload: false});
      dispatch({type: SET_ORDER_DETAILS, payload: null});
    }
  }, [route]);

  useEffect(() => {
    if (categories.length) {
      setCategoryListLoading(true);
      setCategoryList(categories);
      setCategoryListLoading(false);
    }
  }, [categories]);

  useEffect(() => {
    if (modalState && isAdd) {
      const {_id, name, options, basePrice, restaurant} = modalState;

      const foodItem = {
        food: _id,
        name,
        basePrice,
        options,
        optionsPrice: 0,
        price: parseFloat(parseFloat(basePrice * count).toFixed(2)),
        quantity: count,
        isAdded: false,
        uniqueId:
          name.substring(0, 3).toUpperCase() +
          Math.floor(Math.random() * 1000) +
          1,
      };

      let updateCartList;

      if (cartList) {
        updateCartList = {...cartList};
      } else {
        updateCartList = {
          note: '',
          customer: '',
          restaurant,
          count: 0,
          subTotal: 0,
          foodItems: [],
        };
      }

      updateCartList.foodItems.push(foodItem);

      let subTotal = 0;
      updateCartList.foodItems.forEach(item => {
        subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
      });

      updateCartList.subTotal = subTotal;
      updateCartList.count = updateCartList.foodItems.length;

      dispatch({type: SET_CART_LIST, payload: updateCartList});
    }
  }, [count, modalState, isAdd]);

  useEffect(() => {
    setIsViewBasket(!!cartList);
  }, [cartList]);

  useEffect(() => {
    if (activeCategory) {
      setFoodItemLoading(true);
      const foodItems = foods.filter(item => item.category === activeCategory);
      setFoodItemList(foodItems);
      setFoodItemLoading(false);
    }
  }, [activeCategory]);

  const updateServiceType = key => {
    const updateServiceListArea = serviceListArea.map((item, index) => {
      item.isActive = key === index;
      return item;
    });

    const activeService = updateServiceListArea.find(
      item => item.isActive === true,
    );
    dispatch({type: SET_SERVICE, service_type: activeService.key});
    setServiceListArea(updateServiceListArea);

    if (
      activeService.key === 'dine_in' &&
      route.params &&
      route.params.orderDetailsId
    ) {
      dispatch({type: SET_SAVED_DINE_IN, payload: true});
    } else {
      dispatch({type: SET_SAVED_DINE_IN, payload: false});
    }
  };

  const toggleCategory = activeCategory => {
    const updateCategoryList = categoryList.map(item => {
      if (item._id === activeCategory) {
        item.isActive = !item.isActive;
      } else item.isActive = false;
      return item;
    });

    setCategoryList(updateCategoryList);
    setActiveCategory(activeCategory);
  };

  const toggleAddToCart = (foodItemList, index) => {
    const foodItem = foodItemList.find((item, key) => key === index);

    setState({
      count: 1,
      isAdd: false,
    });

    if (foodItem.options.length > 0) {
      setSpecialOfferModalState(foodItem);
      setIsSpecialOfferModalVisible(!isSpecialOfferModalVisible);
    } else {
      setModalState(foodItem);
      setModalVisible(!isModalVisible);
    }
  };

  const hideCategoryList = () => {
    const updateCategoryList = categoryList.map(item => {
      item.isActive = false;
      return item;
    });

    setCategoryList(updateCategoryList);
    setActiveCategory(null);
  };

  const renderRestaurantInfo = restaurantInfo && (
    <>
      <ScrollView
        style={[container, paddingHorizontal0]}
        showsVerticalScrollIndicator={false}>
        <ServiceTypeArea
          serviceListArea={serviceListArea}
          updateServiceType={updateServiceType}
        />

        <CategoryArea
          categoryList={categoryList}
          activeCategory={activeCategory}
          toggleCategory={toggleCategory}
          foodItemLoading={foodItemLoading}
          foodItemList={foodItemList}
          toggleAddToCart={toggleAddToCart}
          isViewBasket={isViewBasket}
          categoryListLoading={categoryListLoading}
        />
      </ScrollView>

      {isViewBasket && (
        <BasketArea
          navigation={navigation}
          totalPrice={cartList ? cartList.subTotal : 0}
          isRestaurantOn={isRestaurantOn}
        />
      )}

      <AddToCartModal
        state={state}
        setState={setState}
        modalState={modalState}
        isModalVisible={isModalVisible}
        setModalVisible={setModalVisible}
        setIsViewBasket={setIsViewBasket}
        hideCategoryList={hideCategoryList}
      />

      <SpecialOfferModal
        cartList={cartList}
        dispatch={dispatch}
        specialOfferModalState={specialOfferModalState}
        isSpecialOfferModalVisible={isSpecialOfferModalVisible}
        setIsSpecialOfferModalVisible={setIsSpecialOfferModalVisible}
        hideCategoryList={hideCategoryList}
      />
    </>
  );

  return restaurantInfoLoading ? <Loader /> : renderRestaurantInfo;
};

export default RestaurantDetails;
