import React from "react";
import { Text, TouchableOpacity } from "react-native";

import styles from "./styles";

export default function BasketArea(props) {
    const {
        navigation,
        totalPrice,
        isRestaurantOn,
    } = props;

    const {
        viewBasketArea,
        viewBasketText,
    } = styles;

    const onClickViewBasket = () => {
        if (!isRestaurantOn) {
            alert("Sorry, we are closed now!Try again later.");
            return;
        }

        navigation.navigate("AddToCart");
    };

    return <TouchableOpacity style={viewBasketArea} onPress={() => onClickViewBasket()}>
        <Text style={viewBasketText}>View Basket</Text>
        <Text style={viewBasketText}>£{totalPrice}</Text>
    </TouchableOpacity>;
}
