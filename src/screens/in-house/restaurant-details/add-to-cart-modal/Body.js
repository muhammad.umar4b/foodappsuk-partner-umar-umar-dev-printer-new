import React from "react";
import { Pressable, Text, View } from "react-native";

import Feather from "react-native-vector-icons/Feather";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

export default function Body(props) {
    const {
        state,
        setState,
        modalState,
        isModalVisible,
        setModalVisible,
        setIsViewBasket,
        hideCategoryList
    } = props;

    const {
        modalBody,
        modalContentField,
        addToArea,
        addToButtonArea,
        addToButton,
    } = styles;

    const {
        flexDirectionRow,
        f22,
        justifyCenter,
    } = globalStyles;

    const {
        price,
        description,
    } = modalState;

    const {
        count
    } = state;

    const onClickAddToCart = () => {
        if (count >= 1) {
            const updateState = { ...state };
            updateState.count = count;
            updateState.isAdd = true;
            setState(updateState);
        }

        setModalVisible(!isModalVisible);
        setIsViewBasket(true);
        hideCategoryList();
    };

    return <View style={modalBody}>
        {description !== "" && <Text style={modalContentField}>{description}</Text>}
        <View style={[flexDirectionRow, justifyCenter]}>
            <View style={addToArea}>
                <Pressable onPress={() => count > 1 && setState({...state, count: count - 1})}>
                    <Feather name="minus-circle" size={36} color="#D2181B"/>
                </Pressable>
                <Text style={f22}>
                    {count}
                </Text>
                <Pressable onPress={() => setState({...state, count: count + 1})}>
                    <Feather name="plus-circle" size={36} color="#D2181B"/>
                </Pressable>
            </View>
        </View>
        <Pressable style={addToButtonArea} onPress={() => onClickAddToCart()}>
            <Text style={addToButton}>
                Add For £{parseFloat(parseFloat(price) * count).toFixed(2)}
            </Text>
        </Pressable>
    </View>;
}
