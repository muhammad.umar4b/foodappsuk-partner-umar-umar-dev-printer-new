import React from "react";
import { Text, Pressable, View } from "react-native";

import AntDesign from "react-native-vector-icons/AntDesign";

import styles from "../styles";

export default function Header(props) {
    const {
        modalState,
        isModalVisible,
        setModalVisible,
        hideCategoryList,
    } = props;

    const {
        modalHeaderArea,
        modalHeaderField,
        modalCloseIconArea,
    } = styles;

    const {
        name,
    } = modalState;

    return <View style={modalHeaderArea}>
        <Text style={modalHeaderField}>
            {name}
        </Text>
        <Pressable style={modalCloseIconArea} onPress={() => {
            setModalVisible(!isModalVisible);
            hideCategoryList();
        }}>
            <AntDesign name="close" size={18} color="#fff" />
        </Pressable>
    </View>;
}
