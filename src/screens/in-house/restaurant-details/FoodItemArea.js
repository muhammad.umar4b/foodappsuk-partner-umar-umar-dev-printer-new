import React from "react";
import { Text, TouchableOpacity, View } from "react-native";

import Loader from "../../../utilities/Loader";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function FoodItemArea(props) {
    const {
        foodItemList,
        toggleAddToCart,
        foodItemLoading,
    } = props;

    const {
        foodItemArea,
        detailsArea,
        detailsAreaLeft,
        detailsAreaTitle,
        detailsAreaAmount,
        detailsAreaDescription,
    } = styles;

    const {
        paddingTop1,
        paddingBottom1,
    } = globalStyles;


    return <View style={foodItemArea}>
        {foodItemLoading ? <Loader style={[paddingTop1, paddingBottom1]} /> :
            foodItemList.map((item, index) => (
                <TouchableOpacity style={detailsArea} key={index} onPress={() => toggleAddToCart(foodItemList, index)}>
                    <View style={detailsAreaLeft}>
                        <Text style={detailsAreaTitle}>{item.name}</Text>
                        {item.description !== "" && <Text style={detailsAreaDescription}>{item.description}</Text>}
                        <Text style={detailsAreaAmount}>£{item.price}</Text>
                    </View>
                </TouchableOpacity>
            ))
        }
    </View>;
}
