import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const styles = StyleSheet.create({
    modalHeaderArea: {
        flexDirection: "row",
        backgroundColor: "#fff",
        justifyContent: "space-between",
        paddingHorizontal: wp("4%"),
        paddingVertical: hp("2%"),
        borderBottomWidth: 1,
        borderBottomColor: "#CDC9C9",
    },
    modalHeaderField: {
        fontSize: wp("5%"),
        color: "#000",
        fontWeight: "600",
    },
    modalCloseIconArea: {
        backgroundColor: "#D2181B",
        width: 30,
        height: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 3,
    },
    modalBody: {
        backgroundColor: "#fff",
        paddingHorizontal: wp("4%"),
        paddingBottom: hp("2%"),
    },
    modalContentField: {
        textAlign: "justify",
        lineHeight: 23,
        fontSize: 16,
        color: "#000000",
        paddingTop: hp("1%"),
    },
    specialOfferBlockArea: {
        marginTop: hp("2%"),
        paddingVertical: hp("1%"),
        borderRadius: 8,
        borderColor: "#eee",
        borderWidth: 1,
    },
    specialOfferBlockAreaHeader: {
        borderBottomColor: "#eee",
        borderBottomWidth: 1,
        paddingBottom: hp("1%"),
        paddingHorizontal: wp("2%"),
    },
    specialOfferBlockAreaBody: {
        paddingHorizontal: wp("2%"),
        flexDirection: "row",
        justifyContent: "space-between",
    },
    specialOfferBlockAreaBodyLabel: {
        paddingTop: hp("1%"),
        fontSize: 16,
    },
    addToArea: {
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingTop: hp("2%"),
    },
    addToButtonArea: {
        borderRadius: 8,
        backgroundColor: "#D2181B",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("1%"),
    },
    addToButton: {
        color: "#fff",
        textAlign: "center",
        fontSize: 18,
        fontWeight: "bold",
    },
    tableArea: {
        marginTop: hp("2%"),
        paddingBottom: hp("2%"),
        borderBottomColor: "#b4b4b4",
        borderBottomWidth: 1,
    },
    tableHead: {
        flexDirection: "row",
        height: 40,
        backgroundColor: "#dfdfe3",
    },
    tableBody: {
        flexDirection: "row",
        backgroundColor: "#ebebef",
    },
    tableText: {
        margin: 6,
        fontWeight: "700",
        textAlign: "center",
    },
    tableTextRight: {
        textAlign: "right",
        paddingRight: wp("5%"),
    },
});

export default styles;
