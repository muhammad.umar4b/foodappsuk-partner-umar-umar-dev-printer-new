import React from "react";
import {Pressable, Text} from "react-native";

import styles from "../styles";
import globalStyles from "../../../../assets/styles/globalStyles";

import {SET_CART_LIST} from "../../../../store/types";

const Footer = (props) => {
    const {
        cartList,
        dispatch,
        tableData,
        totalPrice,
        specialOfferModalState,
        isSpecialOfferModalVisible,
        setIsSpecialOfferModalVisible,
        hideCategoryList
    } = props;

    const {
        addToButtonArea,
        addToButton,
    } = styles;

    const {} = globalStyles;

    const {
        _id,
        name,
        basePrice,
        restaurant,
    } = specialOfferModalState;


    const onClickAddToCart = () => {
        let options = [];
        let optionsPrice = parseFloat(parseFloat(totalPrice - basePrice).toFixed(2));

        tableData.forEach(tableItem => {
            tableItem.tableBodyData.forEach(rowItem => {
                if (rowItem[2]) {
                    options.push(rowItem[0]);
                }
            });
        });

        const foodItem = {
            food: _id,
            name,
            basePrice,
            options,
            optionsPrice,
            price: totalPrice,
            quantity: 1,
            isAdded: false,
            uniqueId: name.substring(0, 3).toUpperCase() + Math.floor(Math.random() * 1000) + 1,
        };

        let updateCartList;

        if (cartList) {
            updateCartList = {...cartList};
        } else {
            updateCartList = {
                note: "",
                customer: "",
                restaurant,
                count: 0,
                subTotal: 0,
                foodItems: [],
            };
        }

        updateCartList.foodItems.push(foodItem);

        let subTotal = 0;
        updateCartList.foodItems.forEach(item => {
            subTotal = parseFloat(parseFloat(subTotal + item.price).toFixed(2));
        });

        updateCartList.subTotal = subTotal;
        updateCartList.count = updateCartList.foodItems.length;

        dispatch({type: SET_CART_LIST, payload: updateCartList});
        setIsSpecialOfferModalVisible(!isSpecialOfferModalVisible);
        hideCategoryList();
    };

    return (
        <Pressable
            style={[addToButtonArea]}
            onPress={() => onClickAddToCart()}
        >
            <Text style={addToButton}>
                Add For £{totalPrice ? totalPrice : basePrice}
            </Text>
        </Pressable>
    );
};

export default Footer;
