import React, {useEffect, useLayoutEffect, useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

import styles from '../../assets/styles/allOrderStyles';
import globalStyles from '../../assets/styles/globalStyles';

// Component
import AllOrder from '../../utilities/all-order/AllOrder';
import PendingOrder from './pending-order/PendingOrder';

const allOrdersTab = 0;
const pendingOrdersTab = 1;

const InHouseOrder = ({navigation, route}) => {
  const [activeTab, setActiveTab] = useState(allOrdersTab);
  const [totalOrder, setTotalOrder] = useState(0);
  const [totalPendingOrder, setTotalPendingOrder] = useState(0);

  const {
    container,
    tabArea,
    tabButton,
    activeTabButton,
    activeTabButtonText,
    tabButtonText,
    continueButton,
    continueText,
  } = styles;

  const {marginRight4} = globalStyles;

  useEffect(() => {
    if (route.params && route.params.pending) {
      setActiveTab(pendingOrdersTab);
    } else setActiveTab(allOrdersTab);
  }, [route, navigation]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={marginRight4}>
          <TouchableOpacity
            style={continueButton}
            onPress={() => navigation.navigate('RestaurantDetails')}>
            <Text style={continueText}>New Order</Text>
          </TouchableOpacity>
        </View>
      ),
    });
  }, [navigation]);
  const activeTabStyle = index =>
    activeTab === index ? [tabButton, activeTabButton] : [tabButton];
  const activeTabTextStyle = index =>
    activeTab === index
      ? [tabButtonText, activeTabButtonText]
      : [tabButtonText];

  return (
    <View style={container}>
      <View style={[tabArea]}>
        <TouchableOpacity
          style={activeTabStyle(allOrdersTab)}
          onPress={() => setActiveTab(allOrdersTab)}>
          <Text style={activeTabTextStyle(allOrdersTab)}>
            All Orders {totalOrder ? `(${totalOrder})` : ''}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={activeTabStyle(pendingOrdersTab)}
          onPress={() => setActiveTab(pendingOrdersTab)}>
          <Text style={activeTabTextStyle(pendingOrdersTab)}>
            Pending Orders {totalPendingOrder ? `(${totalPendingOrder})` : ''}
          </Text>
        </TouchableOpacity>
      </View>
      {activeTab === allOrdersTab && (
        <AllOrder
          navigation={navigation}
          isInHouse={true}
          setTotalOrder={setTotalOrder}
          route={route}
        />
      )}
      {activeTab === pendingOrdersTab && (
        <PendingOrder
          navigation={navigation}
          setTotalPendingOrder={setTotalPendingOrder}
          route={route}
        />
      )}
    </View>
  );
};

export default InHouseOrder;
