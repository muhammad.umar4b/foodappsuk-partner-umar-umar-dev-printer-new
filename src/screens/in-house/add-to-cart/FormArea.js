import React from "react";
import {Text, TextInput, TouchableOpacity, View} from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";
import {SET_CART_LIST} from "../../../store/types";

export default function FormArea(props) {
    const {
        state,
        cartList,
        dispatch,
        navigation
    } = props;

    const {
        kitchenNoteText,
        kitchenNoteInput,
        extraKitchenNote,
        kitchenNoteButton,
        kitchenNoteButtonArea,
    } = styles;

    const {
        paddingHorizontal5,
        bgDisabledGrey,
    } = globalStyles;

    const {
        subTotal
    } = state;

    return <View style={paddingHorizontal5}>
        <View>
            <Text style={extraKitchenNote}>Extra Allergy Note</Text>
            <TextInput
                value={cartList ? cartList.note : ''}
                onChangeText={value => dispatch({type: SET_CART_LIST, payload: {...cartList, note: value}})}
                style={kitchenNoteInput}
                keyboardType={"default"}
                editable={!!cartList}
            />
        </View>
        <View style={kitchenNoteButtonArea}>
            <TouchableOpacity
                style={[kitchenNoteButton, subTotal === 0 && bgDisabledGrey]}
                onPress={() => navigation.navigate("Payment")}
                disabled={subTotal === 0}
            >
                <Text style={kitchenNoteText}>Check out £{subTotal}</Text>
            </TouchableOpacity>
        </View>
    </View>;
}