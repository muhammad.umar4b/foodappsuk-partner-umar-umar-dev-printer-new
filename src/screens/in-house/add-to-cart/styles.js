import {StyleSheet} from "react-native";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: "space-between",
    },
    height55: {
        height: hp("55%"),
    },
    addedCartAreaHeader: {
        paddingTop: hp("2%"),
        paddingBottom: hp("2%"),
        borderBottomColor: "#dcd6d6",
        borderBottomWidth: 1,
    },
    addedCartAreaHeaderText: {
        paddingLeft: wp("5%"),
        fontSize: 18,
        fontWeight: "700",
        color: "#555555",
    },
    addedListArea: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("1%"),
        borderBottomColor: "#dcd6d6",
        borderBottomWidth: 1,
    },
    addedListAreaContent: {
        flexDirection: "row",
    },
    addButtonArea: {
        backgroundColor: "#d9d3d3",
        borderRadius: 50,
    },
    addButtonAreaText: {
        paddingTop: hp("1%"),
        textAlign: "center",
        fontSize: 18
    },
    addButtonAreaPlusIcon: {
        paddingTop: hp("0.5%"),
        fontWeight: "bold",
    },
    addButtonAreaMinusIcon: {
        paddingBottom: hp("0.5%"),
        fontWeight: "bold",
    },
    contentArea: {
        paddingLeft: wp("5%"),
    },
    contentTitle: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#555555",
    },
    contentSize: {
        fontSize: 14,
        color: "#4e4c4c",
    },
    contentDescription: {
        fontSize: 14,
        paddingTop: hp("1%"),
        color: "#555555",
    },
    contentPrice: {
        fontWeight: "bold",
        color: "#555555",
        fontSize: 18,
    },
    extraKitchenNote: {
        paddingVertical: wp("2%"),
        fontSize: 16,
    },
    kitchenNoteInput: {
        paddingVertical: hp("1%"),
        paddingLeft: wp("4%"),
        width: wp("90%"),
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
    },
    kitchenNoteButtonArea: {
        flexDirection: "row",
        justifyContent: "center",
        paddingVertical: hp("2%"),
    },
    kitchenNoteButton: {
        backgroundColor: "#D2181B",
        paddingVertical: hp("1.5%"),
        paddingHorizontal: wp("10%"),
        borderRadius: 8,
    },
    kitchenNoteText: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center",
    },
});

export default styles;
