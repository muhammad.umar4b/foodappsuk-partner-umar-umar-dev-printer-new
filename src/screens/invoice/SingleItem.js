import React from "react";
import { Text, View } from "react-native";

import globalStyles from "../../assets/styles/globalStyles";

const SingleItem = ({ label, price }) => {
    const {
        f16,
        flexDirectionRow,
        justifyBetween,
        paddingBottom1,
    } = globalStyles;

    const itemStyle = [flexDirectionRow, justifyBetween, paddingBottom1];

    return (
        <View style={itemStyle}>
            <Text style={f16}>{label}</Text>
            <Text style={f16}>{price.toFixed(2)}</Text>
        </View>
    );
};


export default SingleItem;
