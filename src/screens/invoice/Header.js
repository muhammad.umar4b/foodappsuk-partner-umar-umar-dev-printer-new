import React from "react";
import { Text, View } from "react-native";

import globalStyles from "../../assets/styles/globalStyles";

const Header = ({ restaurantData }) => {
    const {
        paddingBottom3,
        paddingTop2,
        f18,
        f22,
        textCenter,
    } = globalStyles;

    const {
        name,
    } = restaurantData;

    return (
        <View style={[paddingTop2, paddingBottom3]}>
            <Text style={[f22, textCenter]}>
                {name}
            </Text>
            <Text style={[f18, textCenter]}>
                Last Week Invoice
            </Text>
        </View>
    );
};


export default Header;
