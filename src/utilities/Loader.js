import React from "react";
import { ActivityIndicator, View, StyleSheet } from "react-native";

const Loader = (props) => {
    return (
        <View
            style={styles.loadingArea}
            {...props}
        >
            <ActivityIndicator size="large" color="#D2181B" />
        </View>
    );
};

const styles = StyleSheet.create({
    loadingArea: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
    },
});

export default Loader;
