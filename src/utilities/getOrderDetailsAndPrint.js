import axios from 'axios';

import {apiBaseUrl} from '../config/index-example';
import {printContent} from './order-print';

export const getOrderDetailsAndPrint = async (
  id,
  restaurantData = null,
  isPrint = false,
) => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}order/get-single-order/${id}`,
    );
    if (response.data) {
      if (isPrint && restaurantData)
        printContent(response.data.data, restaurantData);
      return response.data.data;
    }
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
    }
  }
};
