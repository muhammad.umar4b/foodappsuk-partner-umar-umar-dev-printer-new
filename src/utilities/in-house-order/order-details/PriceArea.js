import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";

import styles from "./styles";
import globalStyles from "../../../assets/styles/globalStyles";

export default function PriceArea({ orderDetails }) {
    const [finalTotalPrice, setFinalTotalPrice] = useState(0);

    const {
        totalPriceArea,
    } = styles;

    const {
        fw700,
        flexDirectionRow,
        justifyBetween,
        paddingLeft5,
        paddingBottom1,
        paddingRight7,
        paddingTop1,
    } = globalStyles;

    const {
        subTotal,
        orderType,
        serviceCharge,
        paymentMethod,
        deliveryCharge,
        totalPrice,
        partnerFACharge,
    } = orderDetails;

    useEffect(() => {
        if (totalPrice && orderType) {
            let updateFinalTotalPrice = totalPrice;
            if (orderType !== "Dine In") updateFinalTotalPrice += partnerFACharge;
            setFinalTotalPrice(updateFinalTotalPrice);
        }
    }, [totalPrice, orderType]);

    return (
        <>
            <View style={paddingTop1}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Sub Total
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{subTotal ? parseFloat(subTotal).toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>

            {orderType === "Dine In" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Service Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{serviceCharge ? parseFloat(serviceCharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            {orderType === "Home Delivery" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Delivery Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{deliveryCharge ? parseFloat(deliveryCharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            {orderType !== "Dine In" &&
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Order Charge (+)
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{partnerFACharge ? parseFloat(partnerFACharge).toFixed(2) : "0.00"}
                    </Text>
                </View>
            }

            <View style={totalPriceArea}>
                <View style={[flexDirectionRow, justifyBetween]}>
                    <Text style={[fw700, paddingLeft5, paddingBottom1]}>
                        Total Payment ({paymentMethod})
                    </Text>
                    <Text style={[fw700, paddingRight7]}>
                        £{finalTotalPrice ? parseFloat(finalTotalPrice).toFixed(2) : "0.00"}
                    </Text>
                </View>
            </View>
        </>
    );
}
