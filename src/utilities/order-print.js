import { ToastAndroid } from "react-native";

import moment from "moment";
import AsyncStorage from "@react-native-async-storage/async-storage";
import BluetoothStateManager from "react-native-bluetooth-state-manager";
import { BLEPrinter } from "react-native-thermal-receipt-printer";

const company = "FoodApps";
const companyName = `<CD>${company}</CD>\n`;
// const companyMoto = "Order Food Online/Book a Table";
const companyInfo = `${companyName}`;

const getPrintContentGapText = (size) => {
    let i;
    let gap = "";
    for (i = 0; i < size; i++) {
        gap += " ";
    }
    return gap;
};

const getPrintContentGap = (item, qtyAndPriceText) => {
    const { price } = item;
    const leftContentLength = qtyAndPriceText.toString().length;
    const rightContentLength = (price.toString()).length === 1 ? ((price.toString()).length + 3) : ((parseFloat(price).toFixed(2)).toString()).length;
    return getPrintContentGapText(32 - (leftContentLength + rightContentLength)); // Here 32 for page maximum character size
};

const getSubtotalContentGap = (content, price) => {
    const leftContentLength = content.length;
    const rightContentLength = (price.toString()).length === 1 ? ((price.toString()).length + 3) : ((parseFloat(price).toFixed(2)).toString()).length;
    return getPrintContentGapText(32 - (leftContentLength + rightContentLength)); // Here 32 for page maximum character size
};

export const printContent = (orderInfo, restaurantInfo) => {
    BluetoothStateManager.enable().then(() => {
        BLEPrinter.init().then(() => {
            BLEPrinter.getDeviceList().then(async () => {
                const activeBluetoothPrinter = await AsyncStorage.getItem(`activeBluetoothPrinter`);
                if (activeBluetoothPrinter !== null) {
                    BLEPrinter.connectPrinter(activeBluetoothPrinter).then(value => {
                        if (value) {
                            const {
                                createdAt,
                                orderNumber,
                                cartItems,
                                totalPrice,
                                orderType,
                                deliveryCharge,
                                mobileNo,
                                paymentMethod,
                                customerName,
                                deliveredAt,
                                tableNo,
                                subTotal,
                                serviceCharge,
                                postCode,
                                partnerFACharge,
                            } = orderInfo;

                            let finalTotalPrice = totalPrice;
                            if (orderType !== "Dine In") finalTotalPrice += partnerFACharge;

                            const cartItem = JSON.parse(cartItems);
                            const { note, foodItems } = cartItem;

                            const seperator = "--------------------------------";

                            /*Print Text PrinterHeader*/
                            const restaurantNameText = `<C>${restaurantInfo.name || "N/A"}</C>\n`;
                            const restaurantLocationText = `<C>${restaurantInfo.address || "N/A"}</C>\n`;
                            const restaurantPostCodeText = `<C>${restaurantInfo.postCode || "N/A"}</C>\n`;
                            let orderDateAndOrderNoText = `<C>${createdAt ? moment(createdAt).format("DD/MM/YYYY, hh:mm A") : "N/A"}</C>\n`;
                            orderDateAndOrderNoText += `<C>Order No: ${orderNumber || "N/A"}</C>\n\n`;

                            /* Description Body */
                            const descriptionHeader = `Description(QTY)     Amount(GBP)\n${seperator}\n`;

                            /* Table Body */
                            let descriptionBody = "";
                            foodItems.forEach((item, index) => {
                                // const qtyAndPriceText = `${item.quantity}*${item.basePrice.toFixed(2)}`;
                                // descriptionBody += `${item.name}\n`;
                                // if (item.options.length > 0) descriptionBody += `${item.options.join(", ")}\n`;
                                // descriptionBody += `${qtyAndPriceText}${getPrintContentGap(item, qtyAndPriceText)}${item.price.toFixed(2)}\n`;

                                const qtyAndName = `${item.quantity}*${item.name}`;
                                descriptionBody += `<M>${qtyAndName}${getPrintContentGap(item, qtyAndName)}${item.price.toFixed(2)}</M>\n`;
                                if (item.options.length > 0) descriptionBody += `<M>${item.options.join(", ")}</M>\n`;
                                if (index + 1 < foodItems.length) descriptionBody += "\n";
                            });

                            /* Total Body */
                            const subTotalText = `Sub Total${getSubtotalContentGap("Sub Total", subTotal)}${parseFloat(subTotal).toFixed(2)}\n`;
                            const serviceChargeText = `Service Charge (+)${getSubtotalContentGap("Service Charge (+)", serviceCharge)}${parseFloat(serviceCharge).toFixed(2)}\n`;
                            const deliveryChargeText = (orderType === "Home Delivery") ? `Delivery Charge (+)${getSubtotalContentGap("Delivery Charge (+)", deliveryCharge)}${parseFloat(deliveryCharge).toFixed(2)}\n` : "";
                            const orderChargeText = orderType !== "Dine In" ? `Order Charge (+)${getSubtotalContentGap("Order Charge (+)", partnerFACharge)}${parseFloat(partnerFACharge).toFixed(2)}\n` : "";
                            const totalPaymentText = `<M>Total Payment (${paymentMethod})${getSubtotalContentGap(`Total Payment (${paymentMethod})`, finalTotalPrice)}${parseFloat(finalTotalPrice).toFixed(2)}</M>\n\n`;

                            /* Kitchen Notes */
                            const kitchenNotesText = `Kitchen Notes: ${note}`;

                            /* Address */
                            const deliveryToUser = `<C>Delivery To: ${customerName || "N/A"}</C>\n`;
                            const deliveryToAddress = `<CB>${deliveredAt || "N/A"}${postCode ? `, ${postCode}` : ""}</CB>\n`;
                            const collectionFor = `<C>Collection For: ${customerName || "N/A"}</C>\n`;
                            const tableNoText = `<CD>Table No: ${tableNo || "N/A"}</CD>\n`;
                            const phoneNumberText = mobileNo ? `<CB>Guest No: ${mobileNo || "N/A"}</CB>` : "";

                            const printTextHeader = `${companyInfo}${restaurantNameText}${restaurantLocationText}${restaurantPostCodeText}${orderDateAndOrderNoText}`;
                            const printTextDescriptionBody = `${descriptionHeader}${descriptionBody}${seperator}\n`;
                            const isServiceChargeAvailable = orderType === "Dine In" ? serviceChargeText : "";
                            const subtotalBody = `${subTotalText}${isServiceChargeAvailable}${deliveryChargeText}${orderChargeText}${seperator}\n${totalPaymentText}${kitchenNotesText}\n`;

                            let printAddress = "";

                            if (orderType === "Home Delivery") {
                                printAddress = `${deliveryToUser}${deliveryToAddress}`;
                            } else if (orderType === "Collection") {
                                printAddress = `${collectionFor}`;
                            } else if (["Dine In", "Counter"].includes(orderType)) {
                                printAddress = tableNoText;
                            }

                            const printText = `${printTextHeader}${printTextDescriptionBody}${subtotalBody}${printAddress}${phoneNumberText}\n\n\n\n\n\n\n`;

                            // console.log(printText);

                            BLEPrinter.printBill(printText);
                            ToastAndroid.show("Printing completed", ToastAndroid.CENTER);
                        }
                    });
                } else {
                    ToastAndroid.show("Set your printer first", ToastAndroid.CENTER);
                }
            });
        });
    });
};

export const printUserContent = (orderInfo, restaurantInfo) => {
    BluetoothStateManager.enable().then(() => {
        BLEPrinter.init().then(() => {
            BLEPrinter.getDeviceList().then(async () => {
                const activeBluetoothPrinter = await AsyncStorage.getItem(`activeBluetoothPrinter`);
                if (activeBluetoothPrinter !== null) {
                    BLEPrinter.connectPrinter(activeBluetoothPrinter).then(value => {
                        if (value) {
                            const {
                                createdAt,
                                orderNumber,
                                cartItems,
                                totalPrice,
                                orderType,
                                deliveryCharge,
                                mobileNo,
                                paymentMethod,
                                customerName,
                                deliveredAt,
                                tableNo,
                                subTotal,
                                serviceCharge,
                                discount,
                                postCode,
                            } = orderInfo;

                            const cartItem = JSON.parse(cartItems);
                            const { note, foodItems } = cartItem;

                            const seperator = "--------------------------------";

                            /*Print Text PrinterHeader*/
                            const restaurantNameText = `<C>${restaurantInfo.name || "N/A"}</C>\n`;
                            const restaurantLocationText = `<C>${restaurantInfo.address || "N/A"}</C>\n`;
                            const restaurantPostCodeText = `<C>${restaurantInfo.postCode || "N/A"}</C>\n`;
                            let orderDateAndOrderNoText = `<C>${createdAt ? moment(createdAt).format("DD/MM/YYYY, hh:mm A") : "N/A"}</C>\n`;
                            orderDateAndOrderNoText += `<C>Order No: ${orderNumber || "N/A"}</C>\n\n`;

                            /* Description Body */
                            const descriptionHeader = `Description(QTY)     Amount(GBP)\n${seperator}\n`;

                            /* Table Body */
                            let descriptionBody = "";
                            foodItems.forEach((item, index) => {
                                // const qtyAndPriceText = `${item.quantity}*${item.basePrice.toFixed(2)}`;
                                // descriptionBody += `${item.name}\n`;
                                // if (item.options.length > 0) descriptionBody += `${item.options.join(", ")}\n`;
                                // descriptionBody += `${qtyAndPriceText}${getPrintContentGap(item, qtyAndPriceText)}${item.price.toFixed(2)}\n`;

                                const qtyAndName = `${item.quantity}*${item.name}`;
                                descriptionBody += `<M>${qtyAndName}${getPrintContentGap(item, qtyAndName)}${item.price.toFixed(2)}</M>\n`;
                                if (item.options.length > 0) descriptionBody += `<M>${item.options.join(", ")}</M>\n`;
                                if (index + 1 < foodItems.length) descriptionBody += "\n";
                            });

                            /* Discount Charge */
                            const discountCharge = discount ? (discount.discountValue ? (parseFloat(subTotal) * (parseFloat(discount.discountValue) / 100)).toFixed(2) : "") : "";

                            /* Total Body */
                            const subTotalText = `Sub Total${getSubtotalContentGap("Sub Total", subTotal)}${parseFloat(subTotal).toFixed(2)}\n`;
                            const discountText = discountCharge ? `Discount (-)${getSubtotalContentGap("Discount (-)", discountCharge)}${parseFloat(discountCharge).toFixed(2)}\n` : "";
                            const serviceChargeText = `Service Charge (+)${getSubtotalContentGap("Service Charge (+)", serviceCharge)}${parseFloat(serviceCharge).toFixed(2)}\n`;
                            const deliveryChargeText = (orderType === "Home Delivery") ? `Delivery Charge (+)${getSubtotalContentGap("Delivery Charge (+)", deliveryCharge)}${parseFloat(deliveryCharge).toFixed(2)}\n` : "";
                            const totalPaymentText = `<M>Total Payment (${paymentMethod})${getSubtotalContentGap(`Total Payment (${paymentMethod})`, totalPrice)}${parseFloat(totalPrice).toFixed(2)}</M>\n\n`;

                            /* Kitchen Notes */
                            const kitchenNotesText = `Kitchen Notes: ${note}`;

                            /* Address */
                            const deliveryToUser = `<C>Delivery To: ${customerName || "N/A"}</C>\n`;
                            const deliveryToAddress = `<CB>${deliveredAt || "N/A"}${postCode ? `, ${postCode}` : ""}</CB>\n`;
                            const collectionFor = `<C>Collection For: ${customerName || "N/A"}</C>\n`;
                            const tableNoText = `<CD>Table No: ${tableNo || "N/A"}</CD>\n`;
                            const phoneNumberText = mobileNo ? `<CB>Guest No: ${mobileNo || "N/A"}</CB>` : "";

                            const printTextHeader = `${companyInfo}${restaurantNameText}${restaurantLocationText}${restaurantPostCodeText}${orderDateAndOrderNoText}`;
                            const printTextDescriptionBody = `${descriptionHeader}${descriptionBody}${seperator}\n`;
                            const subtotalBody = `${subTotalText}${discountText}${serviceChargeText}${deliveryChargeText}${seperator}\n${totalPaymentText}${kitchenNotesText}\n`;

                            let printAddress = "";

                            if (orderType === "Home Delivery") {
                                printAddress = `${deliveryToUser}${deliveryToAddress}`;
                            } else if (orderType === "Collection") {
                                printAddress = `${collectionFor}`;
                            } else if (["Dine In", "Counter"].includes(orderType)) {
                                printAddress = tableNoText;
                            }

                            const printText = `${printTextHeader}${printTextDescriptionBody}${subtotalBody}${printAddress}${phoneNumberText}\n\n\n\n\n\n\n`;

                            // console.log(printText);

                            BLEPrinter.printBill(printText);
                            ToastAndroid.show("Printing completed", ToastAndroid.CENTER);
                        }
                    });
                } else {
                    ToastAndroid.show("Set your printer first", ToastAndroid.CENTER);
                }
            });
        });
    });
};
