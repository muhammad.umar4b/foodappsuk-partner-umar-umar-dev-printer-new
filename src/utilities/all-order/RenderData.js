import React from 'react';
import {Text} from 'react-native';

import globalStyles from '../../assets/styles/globalStyles';
import SingleItem from './SingleItem';

const RenderData = props => {
  const {data, isInHouse, navigation, isLoading} = props;

  const {bgRed, bgSuccess, paddingTop1} = globalStyles;

  const setStatusBgColor = orderStatus => {
    if (orderStatus === 'Accepted') return bgSuccess;
    else if (orderStatus === 'Canceled') return bgRed;
  };

  return data.length > 0 ? (
    isLoading ? (
      data.map(
        (item, index) =>
          index <= 4 && (
            <SingleItem
              navigation={navigation}
              item={item}
              key={index}
              setStatusBgColor={setStatusBgColor}
              isInHouse={isInHouse}
            />
          ),
      )
    ) : (
      data.map((item, index) => (
        <SingleItem
          navigation={navigation}
          item={item}
          key={index}
          setStatusBgColor={setStatusBgColor}
          isInHouse={isInHouse}
        />
      ))
    )
  ) : (
    <Text
      style={{textAlign: 'center', justifyContent: 'center', marginTop: '30%'}}>
      No Order Yet!
    </Text>
  );
};

export default RenderData;
