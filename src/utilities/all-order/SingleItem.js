import React, { useEffect, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";

import moment from "moment";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

const SingleItem = (props) => {
    const [finalTotalPrice, setFinalTotalPrice] = useState(0);

    const {
        navigation,
        item,
        index,
        isInHouse,
        setStatusBgColor,
    } = props;

    const {
        orderNumber,
        createdAt,
        orderStatus,
        paymentMethod,
        totalPrice,
        orderType,
        tableNo,
        partnerFACharge
    } = item;

    const {
        statusButtonText,
        totalAmountArea,
    } = styles;

    const {
        card,
        boxShadow,
        marginTop2,
        flexDirectionRow,
        justifyBetween,
        textGrey,
        marginLeft2,
        paddingTop1,
        paddingBottom2,
        textCapitalize,
    } = globalStyles;

    useEffect(() => {
        if (totalPrice && orderType) {
            let updateFinalTotalPrice = totalPrice;
            if (orderType !== "Dine In") updateFinalTotalPrice += partnerFACharge;
            setFinalTotalPrice(updateFinalTotalPrice);
        }
    }, [totalPrice, orderType]);

    const orderDetailsPath = isInHouse ? "InHouseOrderDetails" : "MyOrderDetails";

    return (
        <TouchableOpacity
            style={[card, boxShadow, marginTop2]}
            onPress={() => navigation.navigate(orderDetailsPath, { id: item._id })}
            key={index}
        >
            <View style={[flexDirectionRow, justifyBetween]}>
                <View style={[flexDirectionRow]}>
                    <Text style={textGrey}>{orderNumber}</Text>
                    <View style={[marginLeft2, setStatusBgColor(orderStatus), { height: 22, borderRadius: 5 }]}>
                        <Text style={statusButtonText}>{orderStatus}</Text>
                    </View>
                </View>
            </View>

            <View style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                <Text>Order Date</Text>
                <Text>{moment(createdAt).format("DD MMM YYYY, hh:mm A") || "N/A"}</Text>
            </View>

            <View
                style={[flexDirectionRow, justifyBetween, paddingTop1]}>
                <Text>Payment Method</Text>
                <Text style={textCapitalize}>{paymentMethod || "N/A"}</Text>
            </View>

            <View
                style={[flexDirectionRow, justifyBetween, paddingTop1, orderType !== "Dine In" && paddingBottom2]}>
                <Text>Order Type</Text>
                <Text style={textCapitalize}>{orderType || "N/A"}</Text>
            </View>

            {orderType === "Dine In" &&
                <View
                    style={[flexDirectionRow, justifyBetween, paddingTop1, paddingBottom2]}>
                    <Text>Table No</Text>
                    <Text style={textCapitalize}>{tableNo || "N/A"}</Text>
                </View>
            }

            <View style={totalAmountArea}>
                <Text>Total Price</Text>
                <Text>£{parseFloat(finalTotalPrice).toFixed(2)}</Text>
            </View>
        </TouchableOpacity>
    );
};

export default SingleItem;
