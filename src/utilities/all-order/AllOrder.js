import React, {useEffect, useState} from 'react';
import {View, SafeAreaView, ScrollView} from 'react-native';

import axios from 'axios';
import {useSelector} from 'react-redux';

import globalStyles from '../../assets/styles/globalStyles';
import {apiBaseUrl} from '../../config/index-example';

import Loader from '../Loader';
import RenderData from './RenderData';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';
import AsyncStorage from '@react-native-async-storage/async-storage';

const AllOrder = ({navigation, setTotalOrder, isInHouse = false, route}) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const state = useSelector(state => state);
  const {
    restaurant: {
      restaurantData: {_id},
    },
  } = state;

  const getMyOrders = async id => {
    try {
      let data = await AsyncStorage.getItem('allOrders');
      let datum = JSON.parse(data);
      if (datum) {
        setData(datum);
        setTotalOrder(datum.length);
        // setIsLoading(false);
      }
      let apiEndPoint = 'order/get-all-restaurant-orders/';
      if (isInHouse) apiEndPoint = 'order/get-orders-by-type/';

      const response = await axios.get(`${apiBaseUrl}${apiEndPoint}${id}`);
      if (response.data) {
        setData(response.data.data);
        setTotalOrder(response.data.data.length);
        AsyncStorage.setItem('allOrders', JSON.stringify(response.data.data));
      }

      setIsLoading(false);
      return true;
    } catch (error) {
      if (error.response.data) {
        console.log(error.response.data);
        if (error.response.data.error)
          showToastWithGravityAndOffset(error.response.data.error);
      }
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getMyOrders(_id).then(res => console.log('ALL ORDERS: ', res));
  }, [_id, route]);

  const {flex1, paddingHorizontal5, paddingBottom3} = globalStyles;

  return (
    <SafeAreaView style={flex1}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[paddingHorizontal5, paddingBottom3]}>
          <RenderData
            navigation={navigation}
            data={data}
            isInHouse={isInHouse}
            isLoading={isLoading}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AllOrder;
