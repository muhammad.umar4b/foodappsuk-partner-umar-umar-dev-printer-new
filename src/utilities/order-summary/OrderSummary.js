import React, {useEffect, useState} from "react";
import {Text, View} from "react-native";

import {useSelector} from "react-redux";

import styles from "./styles";
import globalStyles from "../../assets/styles/globalStyles";

// Component
import Body from "./Body";
import Footer from "./Footer";
import Loader from "../Loader";
import PriceArea from "./PriceArea";

const OrderSummary = ({deliveryCharge = 0, partnerFACharge = 0}) => {
    const tableHead = ["Qty", "Description", "Amount"];
    const [tableData, setTableData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const state = useSelector(state => state);

    const {
        restaurant: {
            cartList,
            service_type,
            restaurantData
        }
    } = state;

    useEffect(() => {
        if (cartList) {
            let updateTableData = [];
            cartList.foodItems.forEach(item => {
                const name = item.options.length > 0 ? `${item.name} (${item.options.join(', ')})` : item.name;
                const arr = [item.quantity, name, parseFloat(item.price).toFixed(2)];
                updateTableData.push(arr);
            });

            setTableData(updateTableData);
            setIsLoading(false);
        }
    }, [cartList]);

    const {
        container,
    } = styles;

    const {
        f18,
        fw700,
        boxShadow,
        flexDirectionRow,
        justifyCenter,
    } = globalStyles;

    return (
        isLoading ? <Loader/> :
            (cartList &&
                <View style={[container, boxShadow]}>
                    <View style={[flexDirectionRow, justifyCenter]}>
                        <Text style={[f18, fw700]}>Your Order</Text>
                    </View>
                    <Body tableHeadData={tableHead} tableData={tableData}/>
                    <PriceArea
                        cartList={cartList}
                        service_type={service_type}
                        deliveryCharge={deliveryCharge}
                        restaurantData={restaurantData}
                        partnerFACharge={partnerFACharge}
                    />
                    <Footer cartList={cartList}/>
                </View>
            )
    );
};

export default OrderSummary;
