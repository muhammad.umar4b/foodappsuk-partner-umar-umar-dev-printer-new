import {combineReducers} from 'redux';
import auth from './reducers/auth';
import restaurant from './reducers/restaurant';
import user from './reducers/user';
// import allOrdersReducer from './reducers/allOrdersReducer';

let reducer;
export default reducer = combineReducers({
  auth: auth,
  restaurant: restaurant,
  user: user,
  //   allOrders: allOrdersReducer,
});
