import {
  ALL_ORDERS_REQUEST,
  ALL_ORDERS_SUCCESS,
  ALL_ORDERS_FAIL,
} from '../constants/allOrdersConstants';
export const allOrderReducer = (state = {allOrders: []}, action) => {
  switch (action.type) {
    case ALL_ORDERS_REQUEST:
      return {loading: true, allOrders: []};
    case ALL_ORDERS_SUCCESS:
      return {loading: false, allOrders: action.payload};
    case ALL_ORDERS_FAIL:
      return {loading: false, error: action.payload};
    default:
      return state;
  }
};
