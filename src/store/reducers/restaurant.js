import {
  SET_CART_LIST,
  SET_SERVICE,
  SET_STATE,
  SET_IN_HOUSE_DINE_IN_ORDER,
  RESTAURANT_INFO_SET,
  RESTAURANT_INFO_SET1,
  SET_RESTAURANT_DATA,
  SET_SAVED_DINE_IN,
  SET_ORDER_DETAILS,
  SET_ALL_CHARGES,
  SET_CATEGORIES,
  SET_FOODS,
} from '../types';

const initialState = {
  restaurantInfo: null,
  restaurantInfo1: null,
  service_type: 'dine_in',
  inHouseDeliveryFee: '',
  inHouseKitchenNotes: '',
  inHouseDineOrderList: null,
  cartList: null,
  restaurantData: null,
  savedDineIn: false,
  orderDetails: null,
  allCharges: null,
  categories: [],
  foods: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case RESTAURANT_INFO_SET:
      return {
        ...state,
        restaurantInfo: action.restaurantInfo,
      };
    case RESTAURANT_INFO_SET1:
      return {
        ...state,
        restaurantInfo1: action.restaurantInfo1,
      };
    case SET_SERVICE:
      return {
        ...state,
        service_type: action.service_type,
      };
    case SET_STATE:
      return {
        ...state,
        [action.payload.name]: action.payload.value,
      };
    case SET_IN_HOUSE_DINE_IN_ORDER:
      return {
        ...state,
        inHouseDineOrderList: action.payload,
      };
    case SET_CART_LIST:
      return {
        ...state,
        cartList: action.payload,
      };
    case SET_RESTAURANT_DATA:
      return {
        ...state,
        restaurantData: action.payload,
      };
    case SET_SAVED_DINE_IN:
      return {
        ...state,
        savedDineIn: action.payload,
      };
    case SET_ORDER_DETAILS:
      return {
        ...state,
        orderDetails: action.payload,
      };
    case SET_ALL_CHARGES:
      return {
        ...state,
        allCharges: action.payload,
      };
    case SET_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
      };
    case SET_FOODS:
      return {
        ...state,
        foods: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
