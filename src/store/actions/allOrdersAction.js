import axios from 'axios';
import {
  ALL_ORDERS_REQUEST,
  ALL_ORDERS_SUCCESS,
  ALL_ORDERS_FAIL,
} from '../constants/allOrdersConstants';

export const allOrders = _id => async dispatch => {
  try {
    dispatch({type: ALL_ORDERS_REQUEST});
    const {data} = await axios.get(
      `https://api.foodapps.uk/api/v1/order/get-all-restaurant-orders/${_id}`,
    );
    dispatch({
      type: ALL_ORDERS_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: ALL_ORDERS_FAIL,
      payload:
        error.response && error.response.data.error
          ? error.response.data.message
          : error.message,
    });
  }
};
