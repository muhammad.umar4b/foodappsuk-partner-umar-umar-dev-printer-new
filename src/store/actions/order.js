import axios from 'axios';

import {apiBaseUrl} from '../../config/index-example';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

export const calculateDeliveryCharge = async payload => {
  try {
    const response = await axios.post(
      `${apiBaseUrl}restaurant-delivery/calculate-delivery-charge`,
      payload,
    );

    if (response.data) {
      return response.data.data;
    } else return null;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return null;
  }
};

export const acceptOrder = async id => {
  try {
    const response = await axios.put(
      `${apiBaseUrl}order/restaurant-accept-order/${id}`,
    );
    console.log(response.data);
    if (response.data) {
      return response.data.data;
    } else return null;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return null;
  }
};

export const declineOrder = async id => {
  try {
    const response = await axios.put(
      `${apiBaseUrl}order/restaurant-cancel-order/${id}`,
    );

    if (response.data) {
      return response.data.success;
    } else return false;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return false;
  }
};
