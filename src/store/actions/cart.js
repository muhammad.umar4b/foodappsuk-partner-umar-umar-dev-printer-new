import {SET_CART_LIST} from "../types";

export const removeFromCart = dispatch => {
    dispatch({type: SET_CART_LIST, payload: null});
};
