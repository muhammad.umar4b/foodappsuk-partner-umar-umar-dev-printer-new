import axios from 'axios';

import {apiBaseUrl} from '../../config/index-example';
import {showToastWithGravityAndOffset} from '../../shared-components/ToastMessage';

export const getOwnerInfo = async () => {
  try {
    const response = await axios.get(
      `${apiBaseUrl}restaurant/get-restaurant-credendital`,
    );

    if (response.data) {
      return response.data.data;
    } else return null;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return null;
  }
};

export const updateOwner = async payload => {
  try {
    const response = await axios.put(
      `${apiBaseUrl}restaurant/update-own-restaurant-credentials`,
      payload,
    );

    if (response.data) {
      return response.data.data;
    } else return null;
  } catch (error) {
    if (error.response.data) {
      console.log(error.response.data);
      if (error.response.data.error)
        showToastWithGravityAndOffset(error.response.data.error);
    }

    return null;
  }
};
