import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: wp("5%"),
    },
    headerTop: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: hp("1%"),
    },
    headerText: {
        color: "#000",
        fontSize: 18,
        fontWeight: "bold",
    },
    f12: {
        fontSize: 12,
    },
    f14: {
        fontSize: 14,
    },
    f16: {
        fontSize: 16,
    },
    f18: {
        fontSize: 18,
    },
    f17: {
        fontSize: 17,
    },
    f20: {
        fontSize: 20,
    },
    f22: {
        fontSize: 22,
    },
    textWhite: {
        color: "#fff",
    },
    textDark: {
        color: "#000",
    },
    textRed: {
        color: "#D2181B",
    },
    textGrey: {
        color: "#555555",
    },
    textLightGrey: {
        color: "#e3e3e3",
    },
    textWarning: {
        color: "#eacd07",
    },
    bgWhite: {
        backgroundColor: "#fff",
    },
    bgRed: {
        backgroundColor: "#D2181B",
    },
    bgGrey: {
        backgroundColor: "#555555",
    },
    bgDisabledGrey: {
        backgroundColor: "#c1b7b7",
    },
    bgLightGrey: {
        backgroundColor: "#e3e3e3",
    },
    bgLime: {
        backgroundColor: "#2d971a",
    },
    bgWarning: {
        backgroundColor: "#F5BD00",
    },
    bgSuccess: {
        backgroundColor: "#48BB78",
    },
    bgPink: {
        backgroundColor: "#e805a9",
    },
    bgCyan: {
        backgroundColor: "#0fdec6",
    },
    flex1: {
        flex: 1,
    },
    flexDirectionRow: {
        flexDirection: "row",
    },
    justifyCenter: {
        justifyContent: "center",
    },
    justifyEnd: {
        justifyContent: "flex-end",
    },
    justifyBetween: {
        justifyContent: "space-between",
    },
    alignItemsBetween: {
        justifyContent: "space-between",
    },
    alignItemsCenter: {
        justifyContent: "center",
    },
    fw600: {
        fontWeight: "600",
    },
    fw700: {
        fontWeight: "700",
    },
    paddingLeft0: {
        paddingLeft: wp("0%"),
    },
    paddingLeft05: {
        paddingLeft: wp("0.5%"),
    },
    paddingLeft1: {
        paddingLeft: wp("1%"),
    },
    paddingLeft2: {
        paddingLeft: wp("2%"),
    },
    paddingLeft5: {
        paddingLeft: wp("5%"),
    },
    paddingRight2: {
        paddingRight: wp("2%"),
    },
    paddingRight7: {
        paddingRight: wp("7%"),
    },
    paddingTop0: {
        paddingTop: hp("0%"),
    },
    paddingTop02: {
        paddingTop: hp("0.2%"),
    },
    paddingTop05: {
        paddingTop: hp("0.5%"),
    },
    paddingTop1: {
        paddingTop: hp("1%"),
    },
    paddingTop2: {
        paddingTop: hp("2%"),
    },
    paddingTop3: {
        paddingTop: hp("3%"),
    },
    paddingTop5: {
        paddingTop: hp("5%"),
    },
    paddingBottom05: {
        paddingBottom: hp("0.5%"),
    },
    paddingBottom1: {
        paddingBottom: hp("1%"),
    },
    paddingBottom2: {
        paddingBottom: hp("2%"),
    },
    paddingBottom3: {
        paddingBottom: hp("3%"),
    },
    paddingBottom20: {
        paddingBottom: 20,
    },
    paddingHorizontal2: {
        paddingHorizontal: wp("2%"),
    },
    paddingHorizontal0: {
        paddingHorizontal: wp("0%"),
    },
    paddingHorizontal3: {
        paddingHorizontal: wp("3%"),
    },
    paddingHorizontal5: {
        paddingHorizontal: wp("5%"),
    },
    paddingHorizontal6: {
        paddingHorizontal: wp("6%"),
    },
    paddingVertical2: {
        paddingVertical: wp("2%"),
    },
    paddingVertical5: {
        paddingVertical: wp("5%"),
    },
    boxShadow: {
        backgroundColor: "#fff",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    elevation3: {
        elevation: 3,
    },
    elevation5: {
        elevation: 5,
    },
    modalView: {
        margin: 0,
        justifyContent: "flex-end",
    },
    marginLeft06: {
        marginLeft: wp("0.6%"),
    },
    marginLeft2: {
        marginLeft: wp("2%"),
    },
    marginLeft3: {
        marginLeft: wp("3%"),
    },
    marginLeft4: {
        marginLeft: wp("4%"),
    },
    marginLeft8: {
        marginLeft: wp("8%"),
    },
    marginRight1: {
        marginRight: wp("1%"),
    },
    marginRight2: {
        marginRight: wp("2%"),
    },
    marginRight3: {
        marginRight: wp("3%"),
    },
    marginRight4: {
        marginRight: wp("4%"),
    },
    marginTop0: {
        marginTop: wp("0%"),
    },
    marginTop1: {
        marginTop: hp("1%"),
    },
    marginTop2: {
        marginTop: hp("2%"),
    },
    marginTop3: {
        marginTop: hp("3%"),
    },
    marginTop5: {
        marginTop: hp("5%"),
    },
    marginBottom1: {
        marginBottom: hp("1%"),
    },
    marginBottom2: {
        marginBottom: hp("2%"),
    },
    marginBottom5: {
        marginBottom: hp("5%"),
    },
    marginHorizontal5: {
        marginHorizontal: wp("5%"),
    },
    circleIconArea: {
        backgroundColor: "#fff",
        width: 24,
        height: 24,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 12,
    },
    card: {
        backgroundColor: "#fff",
        borderRadius: 8,
        paddingVertical: hp("3%"),
        paddingHorizontal: wp("5%"),
    },
    cardHeader: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardHeaderLabel: {
        color: "#555555",
        fontSize: 18,
        fontWeight: "bold",
    },
    borderBottom: {
        borderBottomColor: "#f1ecec",
        borderBottomWidth: 1,
    },
    borderBottomGrey: {
        borderBottomColor: "#b4b4b4",
        borderBottomWidth: 1,
    },
    borderWhite: {
        borderColor: "#ffffff",
    },
    gmailIcon: {
        width: 30,
        height: 30,
    },
    height25p: {
        height: hp("25%"),
    },
    height18: {
        height: 18,
    },
    loadingArea: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    noDataFoundArea: {
        backgroundColor: "#fff",
        paddingHorizontal: wp("3%"),
        paddingVertical: hp("1%"),
        borderRadius: 8,
        marginBottom: hp("2%"),
        marginHorizontal: wp("5%"),
        elevation: 5,
    },
    radioButtonArea: {
        paddingTop: hp("1.5%"),
        flexDirection: "row",
    },
    radioButtonContainer: {
        flexDirection: "row",
        marginRight: wp("3%"),
        paddingBottom: hp("1.5%"),
    },
    radioButton: {
        height: 22,
        width: 22,
        backgroundColor: "#F8F8F8",
        borderRadius: 11,
        borderWidth: 1,
        borderColor: "#D2181B",
        alignItems: "center",
        justifyContent: "center",
    },
    radioButtonIcon: {
        height: 12,
        width: 12,
        borderRadius: 6,
        backgroundColor: "#D2181B",
    },
    radioButtonContentArea: {
        flexDirection: "row",
        paddingLeft: wp("2%"),
    },
    radioButtonImage: {
        width: 30,
        height: 20,
        marginTop: hp("0.5%"),
        marginRight: wp("2%"),
    },
    radioButtonText: {
        fontSize: 18,
    },
    textCenter: {
        textAlign: "center",
    },
    textRight: {
        textAlign: "right",
    },
    textCapitalize: {
        textTransform: "capitalize",
    },
    paymentOptionLabel: {
        fontSize: 18,
        fontWeight: "700",
        color: "#D2181B",
    },
    paymentOptionArea: {
        paddingVertical: hp("2%"),
    },
    cardPaymentArea: {
        marginHorizontal: wp("5%"),
        marginVertical: hp("2%"),
    },
    cardPaymentInputLabel: {
        color: "#555555",
    },
    cardPaymentInputField: {
        paddingVertical: hp("1%"),
        paddingLeft: wp("4%"),
        borderColor: "#d9d3d3",
        borderWidth: 1,
        borderRadius: 8,
    },
    continueButtonArea: {
        flexDirection: "row",
        justifyContent: "center",
        marginTop: hp("3%"),
    },
    continueButton: {
        backgroundColor: "#D2181B",
        paddingVertical: hp("1.5%"),
        paddingHorizontal: wp("10%"),
        borderRadius: 8,
    },
    continueText: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "700",
        textAlign: "center",
    },
    expiryDateModalView: {
        marginVertical: hp("0%"),
        marginHorizontal: wp("5%"),
        justifyContent: "center",
    },
    expiryDateModalHeader: {
        backgroundColor: "#fff",
        paddingVertical: hp("2%"),
        paddingLeft: wp("5%"),
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderWidth: 1,
        borderColor: "#ece6e6",
    },
    expiryDateModalHeaderText: {
        fontSize: 18,
        fontWeight: "700",
        color: "#2e3333",
    },
    expiryDateModalBody: {
        backgroundColor: "#fff",
        paddingVertical: hp("2%"),
        paddingHorizontal: wp("5%"),
        flexDirection: "row",
        justifyContent: "space-between",
    },
    expiryDateModalFooter: {
        paddingVertical: hp("2%"),
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "center",
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
    },
    closeButton: {
        backgroundColor: "#D2181B",
        paddingVertical: hp("1%"),
        paddingHorizontal: wp("5%"),
        borderRadius: 8,
    },
    monthArea: {
        width: wp("35%"),
    },
    monthAreaHeaderText: {
        fontSize: 17,
        paddingBottom: wp("1%"),
        textAlign: "center",
        paddingRight: wp("5%"),
    },
    monthAreaContent: {
        height: 100,
    },
    monthAreaContentBlock: {
        width: wp("30%"),
    },
    monthAreaContentText: {
        fontSize: 16,
        color: "#000",
        textAlign: "center",
        paddingVertical: hp("0.5%"),
        borderRadius: 8,
    },
    monthAreaContentTextActive: {
        backgroundColor: "#D2181B",
        color: "#fff",
    },
});
